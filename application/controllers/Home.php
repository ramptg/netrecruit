<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->_init();
        $this->data = array();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->model('basic_model');
    }

    private function _init() {
        if (!$this->input->is_ajax_request()) {
            $this->output->set_template('front/before_Login2');
        }
    }

    public function index() {
        $this->data['slider'] = $this->db->get_where('slider_content_data', array('status' => '0'))->result();
        $this->data['aboutus'] = $this->db->get('manage_aboutus')->row();
        $this->data['aboutus_section'] = $this->db->get_where('aboutus_section', array('status' => '0'))->result();
        $this->data['carousel_data'] = $this->db->get_where('carousel_data', array('status' => '0'))->result();
        $this->data['contact'] = $this->basic_model->getContactDetail();
        $this->load->view('home', $this->data);
    }

    public function division_executive() {
        $this->data['result'] = $this->basic_model->getDivisions('1');
        $this->data['fresult'] = $this->basic_model->get_multi_fun_expertise('1');
        $this->data['contact'] = $this->basic_model->getContactDetail();
        $this->load->view('division_executive', $this->data);
    }

    public function division_professional() {
        $this->data['result'] = $this->basic_model->getDivisions('2');
        $this->data['fresult'] = $this->basic_model->get_multi_fun_expertise('2');
        $this->data['contact'] = $this->basic_model->getContactDetail();
        $this->load->view('division_professional', $this->data);
    }

    public function division_consultancy() {
        $this->data['result'] = $this->basic_model->getDivisions('3');
        $this->data['fresult'] = $this->basic_model->get_multi_fun_expertise('3');
        $this->data['contact'] = $this->basic_model->getContactDetail();
        $this->load->view('division_consultancy', $this->data);
    }

    public function services_tachnology() {
        $this->data['result'] = $this->basic_model->geService('1');
        $this->data['contact'] = $this->basic_model->getContactDetail();
        $this->load->view('services_tachnology', $this->data);
    }

    public function search() {
        $this->data['result'] = $this->basic_model->geService('2');
        $this->data['contact'] = $this->basic_model->getContactDetail();
        $this->load->view('search', $this->data);
    }

    public function services_advertised() {
        $this->data['result'] = $this->basic_model->geService('3');
        $this->data['contact'] = $this->basic_model->getContactDetail();
        $this->load->view('services_advertised', $this->data);
    }

    public function db_search() {
        $this->data['result'] = $this->basic_model->geService('4');
        $this->data['contact'] = $this->basic_model->getContactDetail();
        $this->load->view('db_search', $this->data);
    }

    public function team() {
        $this->data['result'] = $this->basic_model->getTeamContent();
        $this->data['team'] = $this->basic_model->manageTeamData();
        $this->data['contact'] = $this->basic_model->getContactDetail();
        $this->load->view('team', $this->data);
    }

    public function contactus() {
        $this->data['result'] = $this->basic_model->getContactContent();
        $this->data['contact'] = $this->basic_model->getContactDetail();
        $this->load->view('contactus', $this->data);
    }

    function terms_and_conditions() {
        $this->data['pages'] = $this->basic_model->getPages('term_condition');
        $this->load->view('terms_and_conditions', $this->data);
        //$this->load->view('homepages', $this->data);
    }

    function privacy_policy() {
        $this->data['pages'] = $this->basic_model->getPages('privacy_policy');
        //$this->load->view('homepages', $this->data);
        $this->load->view('privacy_policy', $this->data);
    }

    function cookie_ploicy() {
        $this->data['pages'] = $this->basic_model->getPages('cookie_policy');
        // $this->load->view('homepages', $this->data);
        $this->load->view('cookie_ploicy', $this->data);
    }

    function data_retention() {
        $this->data['pages'] = $this->basic_model->getPages('data_retention');
        $this->load->view('data_retention', $this->data);
        //$this->load->view('homepages', $this->data);
    }

    public function emailsent() {
        $name = $this->input->post('name');
        $email_address = $this->input->post('email');
        $comments = $this->input->post('message');

        $msg = '';
        $msg .= '<html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Untitled Document</title>';
        $msg .= '<link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet">';
        $msg .= '</head>

            <body style="font-family: "Open Sans", sans-serif; font-size:15px;    background-color: #fff;">
            <div style="width:70%; margin:50px auto 0;  background:#f6f6f6; border-radius:5px; padding:50px 10px 10px;border-bottom:2px solid #0b1b46; border-top:2px solid #0b1b46">
            <div style=" text-align:center">
            <h1 style="color:#5f7d95; font-size:18px">Hello, <span style="color:#000">Network Executive</span>, someone requested some information .</h1>
            <p style="font-size:22px"><strong>Requested Informations are below</strong></p>


            </div>
            <div style="width:90%; margin:50px auto; border-bottom:2px solid #0b1b46 border-top:2px solid #0b1b46">
            <table  cellspecing="0" cellpadding="0" class="table table-bordered"  style="width:100%;border-collapse: collapse;">

            <tr>
            <td style="border: 1px solid #d1d1d1; padding:10px; width:200px"><strong >Name</strong> :</td>
            <td style="border: 1px solid #d1d1d1; padding:10px"> <span>' . $name . '</span></td>
            </tr>


            <tr>
            <td style="border: 1px solid #d1d1d1; padding:10px"><strong >Email Address</strong> :</td>
            <td style="border: 1px solid #d1d1d1; padding:10px"><span >' . $email_address . '</span></td>
            </tr>

            <tr>
            <td style="border: 1px solid #d1d1d1; padding:10px"><strong >Comments</strong> :</td>
            <td style="border: 1px solid #d1d1d1; padding:10px"><span >' . $comments . '</span></td>
            </tr>


            </table>

            </div>
            <div style="width:80%; margin:50px auto; text-align:center">
            <img src="http://protechgenie.in/networkexecutive/images/network-executive-black.png" alt="networkexecutive" />
            </div>
            </div>
            </body>
            </html>';

        $config = [
            'smtp_host' => SMTP_HOST,
            'smtp_port' => SMTP_PORT,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASS,
            'mailtype' => MAIL_TYPE,
            'charset' => 'iso-8859-1'
        ];

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        $this->email->from(FROM, 'Network Executive');
        $this->email->to('protechgenietest@gmail.com');
        $this->email->subject("Network Executive");
        $this->email->message($msg);

        if ($this->email->send()) {
            $data = array(
                'name' => $name,
                'email' => $email_address,
                'message' => $comments
            );
            $this->db->insert('contact_details', $data);

            echo json_encode(array('status' => '1', 'message' => 'Form Submitted Successfully,We Will contact you shortly'));
        } else {
            echo json_encode(array('status' => '0', 'message' => 'Please try again later!'));
        }
    }

    public function division_direct() {

        $this->load->view('division_direct', $this->data);
    }

    public function division_recruitment() {

        $this->load->view('division_recruitment', $this->data);
    }

    public function casestudies() {
        $this->data['case_studies'] = $this->db->get_where('case_studies', array('status' => '0'))->result();
        $this->load->view('casestudies', $this->data);
    }

    public function casestudy_project($id = false) {
        $id = $this->uri->segment(2);
        $this->data['case_studies_sub_cat'] = $this->db->get_where('case_studies_sub_cat', array('status' => '0', 'case_study_id' => $id))->result();

        $this->load->view('casestudy_project', $this->data);
    }

    public function casestudy_project1($id = false) {
        $id = $this->uri->segment(2);
        $this->data['result'] = $this->db->get_where('case_studies_sub_cat', array('status' => '0', 'id' => $id))->row();
        $this->load->view('casestudy_project1', $this->data);
    }

}
