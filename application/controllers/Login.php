<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->_init();
        $this->load->library(array('ion_auth', 'form_validation'));
        if ($this->ion_auth->logged_in()) {
            redirect('dashboard');
        }
    }

    private function _init() {
        $this->output->set_template('admin/before_Login');
    }

    public function index() {
        $this->load->view('login');
    }

}
