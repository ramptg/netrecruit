<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->_init();
        $this->data = array();
        $this->load->library(array('ion_auth', 'form_validation'));
        if (!$this->ion_auth->logged_in()) {
            redirect('/');
        }
    }

    private function _init() {
        if (!$this->input->is_ajax_request()) {
            $this->output->set_template('admin/after_Login');
        }
    }

    public function index() {

        $this->load->view('dashboard');
    }

    public function slidercontent() {

        $this->data['active'] = "customer_data";

        if ($this->input->post()) {

            $data = array(
                'first_content' => $this->input->post('first_content'),
                'second_content' => $this->input->post('second_content'),
                'youtube_url' => $this->input->post('youtube_url'),
                'status' => $this->input->post('status'),
                'create_date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('slider_content', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Slider content updated successfully');
                    redirect('dashboard/slidercontent');
                } else {
                    $this->session->set_flashdata('error', 'Slider content updated successfully');
                    redirect('dashboard/slidercontent');
                }
            } else {
                if ($this->db->insert('slider_content', $data)) {
                    $this->session->set_flashdata('message', 'Slider content added successfully');
                    redirect('dashboard/slidercontent');
                } else {
                    $this->session->set_flashdata('message', 'Slider content added unsuccessfully');
                    redirect('dashboard/slidercontent');
                }
            }
        }



        $this->db->order_by('id', 'desc');
        $this->data['result'] = $this->db->get('slider_content')->row();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/slidercontent', $this->data);
    }

    public function manage_whatwedo() {

        $this->data['active'] = "manage_whatwedo";

        if ($this->input->post()) {

            $data = array(
                'title' => $this->input->post('title'),
                'content' => $this->input->post('content'),
                'create_date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('manage_whatwedo', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Content data updated successfully');
                    redirect('dashboard/manage_whatwedo');
                } else {
                    $this->session->set_flashdata('error', 'Content data updated successfully');
                    redirect('dashboard/manage_whatwedo');
                }
            } else {
                if ($this->db->insert('manage_whatwedo', $data)) {
                    $this->session->set_flashdata('message', 'Content data added successfully');
                    redirect('dashboard/manage_whatwedo');
                } else {
                    $this->session->set_flashdata('message', 'Content data added unsuccessfully');
                    redirect('dashboard/manage_whatwedo');
                }
            }
        }



        $this->db->order_by('id', 'desc');
        $this->data['result'] = $this->db->get('manage_whatwedo')->row();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/manage_whatwedo', $this->data);
    }

    public function whatwedo_section($id = false) {

        $this->data['active'] = "whatwedo_section";

        if ($this->input->post()) {

            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                $new_name = time() . str_replace(str_split(' ()\\/,:*?"<>|'), '', $_FILES['file']['name']);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = '*';
                $config['max_filename'] = '255';
                $config['file_name'] = $new_name;

                if (0 < $_FILES['file']['error']) {
                    // $response = array('status'=>301,'message'=>"Error during file upload");
                } else {
                    if (file_exists('upload/' . $new_name)) {
                        /// $response = array('status'=>301,'message'=>"File already exists");
                    } else {
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('file')) {
                            $this->upload->display_errors();
                        } else {
                            $fileName = $new_name;
                        }
                    }
                }
            }

            $data = array(
                'section_title' => $this->input->post('section_title'),
                'section_desc' => $this->input->post('section_desc'),
                'section_content' => $this->input->post('section_content'),
                'status' => $this->input->post('status'),
                'create_date' => date('Y-m-d h:i:s')
            );
            if ($fileName) {
                $data['file'] = $fileName;
            }

            if ($this->input->post('id')) {
                if ($this->db->update('manage_whatwedo_section', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Section data updated successfully');
                    redirect('dashboard/whatwedo_section');
                } else {
                    $this->session->set_flashdata('error', 'Section data updated successfully');
                    redirect('dashboard/whatwedo_section');
                }
            } else {
                if ($this->db->insert('manage_whatwedo_section', $data)) {
                    $this->session->set_flashdata('message', 'Section data added successfully');
                    redirect('dashboard/whatwedo_section');
                } else {
                    $this->session->set_flashdata('message', 'Section data added unsuccessfully');
                    redirect('dashboard/whatwedo_section');
                }
            }
        }

        if ($id) {
            $this->data['result'] = $this->db->get_where('manage_whatwedo_section', array('id' => $id))->row();
        }

        $this->data['section_data'] = $this->db->get('manage_whatwedo_section')->result();


        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/whatwedo_section', $this->data);
    }

    public function manage_wedoit() {

        $this->data['active'] = "manage_wedoit";

        if ($this->input->post()) {

            $data = array(
                'title' => $this->input->post('title'),
                'content' => $this->input->post('content'),
                'create_date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('manage_wedoit', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Content data updated successfully');
                    redirect('dashboard/manage_wedoit');
                } else {
                    $this->session->set_flashdata('error', 'Content data updated successfully');
                    redirect('dashboard/manage_wedoit');
                }
            } else {
                if ($this->db->insert('manage_wedoit', $data)) {
                    $this->session->set_flashdata('message', 'Content data added successfully');
                    redirect('dashboard/manage_wedoit');
                } else {
                    $this->session->set_flashdata('message', 'Content data added unsuccessfully');
                    redirect('dashboard/manage_wedoit');
                }
            }
        }



        $this->db->order_by('id', 'desc');
        $this->data['result'] = $this->db->get('manage_wedoit')->row();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/manage_wedoit', $this->data);
    }

    public function manage_location($id = false) {

        $this->data['active'] = "manage_locations";

        if ($this->input->post()) {


            $data = array(
                'city_name' => $this->input->post('city_name'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'status' => $this->input->post('status'),
                'create_date' => date('Y-m-d h:i:s')
            );


            if ($this->input->post('id')) {
                if ($this->db->update('manage_location', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Location data updated successfully');
                    redirect('dashboard/manage_location');
                } else {
                    $this->session->set_flashdata('error', 'Location data updated successfully');
                    redirect('dashboard/manage_location');
                }
            } else {
                if ($this->db->insert('manage_location', $data)) {
                    $this->session->set_flashdata('message', 'Location data added successfully');
                    redirect('dashboard/manage_location');
                } else {
                    $this->session->set_flashdata('message', 'Location data added unsuccessfully');
                    redirect('dashboard/manage_location');
                }
            }
        }

        if ($id) {
            $this->data['result'] = $this->db->get_where('manage_location', array('id' => $id))->row();
        }
        $this->db->order_by('id', 'desc');
        $this->data['section_data'] = $this->db->get('manage_location')->result();


        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/manage_location', $this->data);
    }

    public function manage_howwedoit() {

        $this->data['active'] = "manage_howwedoit";

        if ($this->input->post()) {

            $data = array(
                'title' => $this->input->post('title'),
                'content' => $this->input->post('content'),
                'create_date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('manage_howwedoit', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Content data updated successfully');
                    redirect('dashboard/manage_howwedoit');
                } else {
                    $this->session->set_flashdata('error', 'Content data updated successfully');
                    redirect('dashboard/manage_howwedoit');
                }
            } else {
                if ($this->db->insert('manage_howwedoit', $data)) {
                    $this->session->set_flashdata('message', 'Content data added successfully');
                    redirect('dashboard/manage_howwedoit');
                } else {
                    $this->session->set_flashdata('message', 'Content data added unsuccessfully');
                    redirect('dashboard/manage_howwedoit');
                }
            }
        }

        $this->db->order_by('id', 'desc');
        $this->data['result'] = $this->db->get('manage_howwedoit')->row();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/manage_howwedoit', $this->data);
    }

    public function section_howwedoit($id = false) {

        $this->data['active'] = "section_howwedoit";

        if ($this->input->post()) {

            $data = array(
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'youtube_url' => $this->input->post('youtube_url'),
                'status' => $this->input->post('status'),
                'date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('section_howwedoit', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Section data updated successfully');
                    redirect('dashboard/section_howwedoit');
                } else {
                    $this->session->set_flashdata('error', 'Section data updated successfully');
                    redirect('dashboard/section_howwedoit');
                }
            } else {
                if ($this->db->insert('section_howwedoit', $data)) {
                    $this->session->set_flashdata('message', 'Section data added successfully');
                    redirect('dashboard/section_howwedoit');
                } else {
                    $this->session->set_flashdata('message', 'Section data added unsuccessfully');
                    redirect('dashboard/section_howwedoit');
                }
            }
        }

        if ($id) {
            $this->data['result'] = $this->db->get_where('section_howwedoit', array('id' => $id))->row();
        }

        $this->data['section_data'] = $this->db->get('section_howwedoit')->result();


        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/section_howwedoit', $this->data);
    }

    public function manage_whodoesit() {

        $this->data['active'] = "manage_whodoesit";

        if ($this->input->post()) {

            $data = array(
                'title' => $this->input->post('title'),
                'content' => $this->input->post('content'),
                'sub_heading' => $this->input->post('sub_heading'),
                'date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('manage_whodoesit', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Content data updated successfully');
                    redirect('dashboard/manage_whodoesit');
                } else {
                    $this->session->set_flashdata('error', 'Content data updated successfully');
                    redirect('dashboard/manage_whodoesit');
                }
            } else {
                if ($this->db->insert('manage_whodoesit', $data)) {
                    $this->session->set_flashdata('message', 'Content data added successfully');
                    redirect('dashboard/manage_whodoesit');
                } else {

                    $this->session->set_flashdata('message', 'Content data added unsuccessfully');
                    redirect('admin/dashboard/manage_whodoesit');
                }
            }
        }

        $this->db->order_by('id', 'desc');
        $this->data['result'] = $this->db->get('manage_whodoesit')->row();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/manage_whodoesit', $this->data);
    }

   

    public function join_the_team($id = false) {

        $this->data['active'] = "join_the_team";

        if ($this->input->post()) {

            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                $new_name = time() . str_replace(str_split(' ()\\/,:*?"<>|'), '', $_FILES['file']['name']);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = '*';
                $config['max_filename'] = '255';
                $config['file_name'] = $new_name;

                if (0 < $_FILES['file']['error']) {
                    // $response = array('status'=>301,'message'=>"Error during file upload");
                } else {
                    if (file_exists('upload/' . $new_name)) {
                        /// $response = array('status'=>301,'message'=>"File already exists");
                    } else {
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('file')) {
                            $this->upload->display_errors();
                        } else {
                            $fileName = $new_name;
                        }
                    }
                }
            }

            $data = array(
                'title' => $this->input->post('title'),
                'sub_title' => $this->input->post('sub_title'),
                'description' => $this->input->post('description'),
                'content' => '',
                'status' => $this->input->post('status'),
                'create_date' => date('Y-m-d h:i:s')
            );
            if ($fileName) {
                $data['file'] = $fileName;
            }

            if ($this->input->post('id')) {
                if ($this->db->update('join_the_team', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/join_the_team');
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/join_the_team');
                }
            } else {
                if ($this->db->insert('join_the_team', $data)) {
                    $this->session->set_flashdata('message', 'Data added successfully');
                    redirect('dashboard/join_the_team');
                } else {
                    $this->session->set_flashdata('message', 'Data added unsuccessfully');
                    redirect('dashboard/join_the_team');
                }
            }
        }

        if ($id) {
            $this->data['result'] = $this->db->get_where('join_the_team', array('id' => $id))->row();
        }

        $this->data['section_data'] = $this->db->get('join_the_team')->result();


        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/join_the_team', $this->data);
    }

    public function lets_talk_content() {

        $this->data['active'] = "lets_talk_content";

        if ($this->input->post()) {

            $data = array(
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'create_date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('lets_talk_content', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/lets_talk_content');
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/lets_talk_content');
                }
            } else {
                if ($this->db->insert('lets_talk_content', $data)) {
                    $this->session->set_flashdata('message', 'Data added successfully');
                    redirect('dashboard/lets_talk_content');
                } else {
                    $this->session->set_flashdata('message', 'Data added unsuccessfully');
                    redirect('dashboard/lets_talk_content');
                }
            }
        }

        $this->db->order_by('id', 'desc');
        $this->data['result'] = $this->db->get('lets_talk_content')->row();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/lets_talk_content', $this->data);
    }

    public function pages($id = false) {

        $this->data['active'] = "lets_talk_content";
        $this->data['page_id'] = $id;
        if ($this->input->post()) {

            $data = array(
                'page_title' => $this->input->post('page_title'),
                'text' => $this->input->post('text'),
                'create_date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('pages', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Pages updated successfully');
                    redirect('dashboard/pages/' . $this->input->post('id'));
                } else {
                    $this->session->set_flashdata('error', 'Pages updated successfully');
                    redirect('dashboard/pages/' . $this->input->post('id'));
                }
            }
        }

        $this->db->order_by('id', 'desc');
        $this->db->where('id', $id);
        $this->data['result'] = $this->db->get('pages')->row();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/pages', $this->data);
    }

    function contact_details() {
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->db->order_by('id', 'desc');
        $this->data['result'] = $this->db->get('contact_details')->result();

        $this->load->view('admin/contact_details', $this->data);
    }

    function delete_contact($id) {
        $this->db->where('id', $id);
        $this->db->delete('contact_details');

        $this->session->set_flashdata('message', 'Contact detail deleted successfully');
        redirect('dashboard/contact_details');
    }

    function delete_whatwedo_section($id) {
        $this->db->where('id', $id);
        $this->db->delete('manage_whatwedo_section');

        $this->session->set_flashdata('message', 'Section detail deleted successfully');
        redirect('dashboard/whatwedo_section');
    }

    function delete_location($id) {
        $this->db->where('id', $id);
        $this->db->delete('manage_location');

        $this->session->set_flashdata('message', 'Loaction detail deleted successfully');
        redirect('dashboard/manage_location');
    }

    function delete_section_howwedoit($id) {
        $this->db->where('id', $id);
        $this->db->delete('section_howwedoit');

        $this->session->set_flashdata('message', 'Section detail deleted successfully');
        redirect('dashboard/section_howwedoit');
    }



    function delete_jointeam($id) {
        $this->db->where('id', $id);
        $this->db->delete('join_the_team');

        $this->session->set_flashdata('message', 'Record deleted successfully');
        redirect('dashboard/join_the_team');
    }

    /*     * ***
     * 
     * 
     */

    public function slider_content($id = false) {

        $this->data['active'] = "slider_content";

        if ($this->input->post()) {

            $data = array(
                'title' => $this->input->post('title'),
                'stitle' => $this->input->post('stitle'),
                'content' => $this->input->post('content'),
                'status' => $this->input->post('status'),
                'create_date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('slider_content_data', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/slider_content');
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/slider_content');
                }
            } else {
                if ($this->db->insert('slider_content_data', $data)) {
                    $this->session->set_flashdata('message', 'Data added successfully');
                    redirect('dashboard/slider_content');
                } else {
                    $this->session->set_flashdata('message', 'Data added unsuccessfully');
                    redirect('dashboard/slider_content');
                }
            }
        }

        if ($id) {
            $this->data['result'] = $this->db->get_where('slider_content_data', array('id' => $id))->row();
        }

        $this->data['section_data'] = $this->db->get('slider_content_data')->result();


        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/slider_content', $this->data);
    }

    function delete_slider_content($id) {
        $this->db->where('id', $id);
        $this->db->delete('slider_content_data');

        $this->session->set_flashdata('message', 'Record deleted successfully');
        redirect('dashboard/slider_content');
    }

    public function manage_aboutus() {

        $this->data['active'] = "manage_aboutus";

        if ($this->input->post()) {

            $data = array(
                'title' => $this->input->post('title'),
                'content' => $this->input->post('content'),
                'create_date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('manage_aboutus', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Content data updated successfully');
                    redirect('dashboard/manage_aboutus');
                } else {
                    $this->session->set_flashdata('error', 'Content data updated successfully');
                    redirect('dashboard/manage_aboutus');
                }
            } else {
                if ($this->db->insert('manage_aboutus', $data)) {
                    $this->session->set_flashdata('message', 'Content data added successfully');
                    redirect('dashboard/manage_aboutus');
                } else {
                    $this->session->set_flashdata('message', 'Content data added unsuccessfully');
                    redirect('dashboard/manage_aboutus');
                }
            }
        }



        $this->db->order_by('id', 'desc');
        $this->data['result'] = $this->db->get('manage_aboutus')->row();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/manage_aboutus', $this->data);
    }

    public function aboutus_section($id = false) {

        $this->data['active'] = "aboutus_section";

        if ($this->input->post()) {

            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                $new_name = time() . str_replace(str_split(' ()\\/,:*?"<>|'), '', $_FILES['file']['name']);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = '*';
                $config['max_filename'] = '255';
                $config['file_name'] = $new_name;

                if (0 < $_FILES['file']['error']) {
                    // $response = array('status'=>301,'message'=>"Error during file upload");
                } else {
                    if (file_exists('upload/' . $new_name)) {
                        /// $response = array('status'=>301,'message'=>"File already exists");
                    } else {
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('file')) {
                            $this->upload->display_errors();
                        } else {
                            $fileName = $new_name;
                        }
                    }
                }
            }

            $data = array(
                'section_title' => $this->input->post('section_title'),
                'section_desc' => $this->input->post('section_desc'),
                'status' => $this->input->post('status'),
                'create_date' => date('Y-m-d h:i:s')
            );
            if ($fileName) {
                $data['file'] = $fileName;
            }

            if ($this->input->post('id')) {
                if ($this->db->update('aboutus_section', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Section data updated successfully');
                    redirect('dashboard/aboutus_section');
                } else {
                    $this->session->set_flashdata('error', 'Section data updated successfully');
                    redirect('dashboard/aboutus_section');
                }
            } else {
                if ($this->db->insert('aboutus_section', $data)) {
                    $this->session->set_flashdata('message', 'Section data added successfully');
                    redirect('dashboard/aboutus_section');
                } else {
                    $this->session->set_flashdata('message', 'Section data added unsuccessfully');
                    redirect('dashboard/aboutus_section');
                }
            }
        }

        if ($id) {
            $this->data['result'] = $this->db->get_where('aboutus_section', array('id' => $id))->row();
        }

        $this->data['section_data'] = $this->db->get('aboutus_section')->result();


        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/aboutus_section', $this->data);
    }

    function delete_aboutus_section($id) {
        $this->db->where('id', $id);
        $this->db->delete('aboutus_section');

        $this->session->set_flashdata('message', 'Section detail deleted successfully');
        redirect('dashboard/aboutus_section');
    }

    public function manage_network_division($slug = false) {

        $this->data['active'] = "manage_network_division";
        if ($this->input->post()) {
            $this->data['slug'] = $slug_n = $this->input->post('slug');
        } else {
            $this->data['slug'] = $slug_n = $slug;
        }
        if ($this->input->post()) {
          
            $data = array(
                'slider_content' => $this->input->post('slider_content'),
                'ftitle' => $this->input->post('ftitle'),
                'fcontent' => $this->input->post('fcontent'),
                'stitle' => $this->input->post('stitle'),
                'scontent' => $this->input->post('scontent'),
                'thtitle' => $this->input->post('thtitle'),
                'thcontent' => $this->input->post('thcontent'),
                'slug' => $slug_n
            );

            if ($this->input->post('slug')) {
                if ($this->db->update('manage_network_division', $data, array('slug' => $slug_n))) {
                   
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/manage_network_division/' . $slug_n);
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/manage_network_division/' . $slug_n);
                }
            }
        }


        $this->data['result'] = $this->db->get_where('manage_network_division', array('id' => $slug_n))->row();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/manage_network_division', $this->data);
    }

    public function exe_multi_fun_expertise($id = false) {

        $this->data['active'] = "exe_multi_fun_expertise";

        if ($this->input->post()) {

            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                $new_name = time() . str_replace(str_split(' ()\\/,:*?"<>|'), '', $_FILES['file']['name']);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = '*';
                $config['max_filename'] = '255';
                $config['file_name'] = $new_name;

                if (0 < $_FILES['file']['error']) {
                    // $response = array('status'=>301,'message'=>"Error during file upload");
                } else {
                    if (file_exists('upload/' . $new_name)) {
                        /// $response = array('status'=>301,'message'=>"File already exists");
                    } else {
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('file')) {
                            $this->upload->display_errors();
                        } else {
                            $fileName = $new_name;
                        }
                    }
                }
            }

            $data = array(
                'title' => $this->input->post('title'),
                'status' => $this->input->post('status'),
                'slug' => $this->input->post('slug'),
                'create_date' => date('Y-m-d h:i:s')
            );
            if ($fileName) {
                $data['file'] = $fileName;
            }

            if ($this->input->post('id')) {
                if ($this->db->update('exe_multi_fun_expertise', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/exe_multi_fun_expertise');
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/exe_multi_fun_expertise');
                }
            } else {
                if ($this->db->insert('exe_multi_fun_expertise', $data)) {
                    $this->session->set_flashdata('message', 'Data added successfully');
                    redirect('dashboard/exe_multi_fun_expertise');
                } else {
                    $this->session->set_flashdata('message', 'Data added unsuccessfully');
                    redirect('dashboard/exe_multi_fun_expertise');
                }
            }
        }

        if ($id) {
            $this->data['result'] = $this->db->get_where('exe_multi_fun_expertise', array('id' => $id))->row();
        }

        $this->data['section_data'] = $this->db->get_where('exe_multi_fun_expertise', array('slug' => '1'))->result();


        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/exe_multi_fun_expertise', $this->data);
    }

    function delete_exe_multi_fun_expertise($id) {
        $this->db->where('id', $id);
        $this->db->delete('exe_multi_fun_expertise');

        $this->session->set_flashdata('message', 'Detail deleted successfully');
        redirect('dashboard/exe_multi_fun_expertise');
    }
    
    
      public function pro_multi_fun_expertise($id = false) {

        $this->data['active'] = "pro_multi_fun_expertise";

        if ($this->input->post()) {

            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                $new_name = time() . str_replace(str_split(' ()\\/,:*?"<>|'), '', $_FILES['file']['name']);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = '*';
                $config['max_filename'] = '255';
                $config['file_name'] = $new_name;

                if (0 < $_FILES['file']['error']) {
                    // $response = array('status'=>301,'message'=>"Error during file upload");
                } else {
                    if (file_exists('upload/' . $new_name)) {
                        /// $response = array('status'=>301,'message'=>"File already exists");
                    } else {
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('file')) {
                            $this->upload->display_errors();
                        } else {
                            $fileName = $new_name;
                        }
                    }
                }
            }

            $data = array(
                'title' => $this->input->post('title'),
                'status' => $this->input->post('status'),
                'slug' => $this->input->post('slug'),
                'create_date' => date('Y-m-d h:i:s')
            );
            if ($fileName) {
                $data['file'] = $fileName;
            }

            if ($this->input->post('id')) {
                if ($this->db->update('exe_multi_fun_expertise', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/pro_multi_fun_expertise');
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/pro_multi_fun_expertise');
                }
            } else {
                if ($this->db->insert('exe_multi_fun_expertise', $data)) {
                    $this->session->set_flashdata('message', 'Data added successfully');
                    redirect('dashboard/pro_multi_fun_expertise');
                } else {
                    $this->session->set_flashdata('message', 'Data added unsuccessfully');
                    redirect('dashboard/pro_multi_fun_expertise');
                }
            }
        }

        if ($id) {
            $this->data['result'] = $this->db->get_where('exe_multi_fun_expertise', array('id' => $id))->row();
        }

        $this->data['section_data'] = $this->db->get_where('exe_multi_fun_expertise', array('slug' => '2'))->result();


        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/pro_multi_fun_expertise', $this->data);
    }
    
      function delete_pro_multi_fun_expertise($id) {
        $this->db->where('id', $id);
        $this->db->delete('exe_multi_fun_expertise');

        $this->session->set_flashdata('message', 'Detail deleted successfully');
        redirect('dashboard/pro_multi_fun_expertise');
    }
    
    public function con_multi_fun_expertise($id = false) {

        $this->data['active'] = "con_multi_fun_expertise";

        if ($this->input->post()) {

            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                $new_name = time() . str_replace(str_split(' ()\\/,:*?"<>|'), '', $_FILES['file']['name']);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = '*';
                $config['max_filename'] = '255';
                $config['file_name'] = $new_name;

                if (0 < $_FILES['file']['error']) {
                    // $response = array('status'=>301,'message'=>"Error during file upload");
                } else {
                    if (file_exists('upload/' . $new_name)) {
                        /// $response = array('status'=>301,'message'=>"File already exists");
                    } else {
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('file')) {
                            $this->upload->display_errors();
                        } else {
                            $fileName = $new_name;
                        }
                    }
                }
            }

            $data = array(
                'title' => $this->input->post('title'),
                'status' => $this->input->post('status'),
                'slug' => $this->input->post('slug'),
                'create_date' => date('Y-m-d h:i:s')
            );
            if ($fileName) {
                $data['file'] = $fileName;
            }

            if ($this->input->post('id')) {
                if ($this->db->update('exe_multi_fun_expertise', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/con_multi_fun_expertise');
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/con_multi_fun_expertise');
                }
            } else {
                if ($this->db->insert('exe_multi_fun_expertise', $data)) {
                    $this->session->set_flashdata('message', 'Data added successfully');
                    redirect('dashboard/con_multi_fun_expertise');
                } else {
                    $this->session->set_flashdata('message', 'Data added unsuccessfully');
                    redirect('dashboard/con_multi_fun_expertise');
                }
            }
        }

        if ($id) {
            $this->data['result'] = $this->db->get_where('exe_multi_fun_expertise', array('id' => $id))->row();
        }

        $this->data['section_data'] = $this->db->get_where('exe_multi_fun_expertise', array('slug' => '3'))->result();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/con_multi_fun_expertise', $this->data);
    }
    
    function delete_con_multi_fun_expertise($id) {
        $this->db->where('id', $id);
        $this->db->delete('exe_multi_fun_expertise');

        $this->session->set_flashdata('message', 'Detail deleted successfully');
        redirect('dashboard/con_multi_fun_expertise');
    }
    
    public function manage_services($slug = false) {

        $this->data['active'] = "manage_services";
        if ($this->input->post()) {
            $this->data['slug'] = $slug_n = $this->input->post('slug');
        } else {
            $this->data['slug'] = $slug_n = $slug;
        }
        if ($this->input->post()) {
            $data = array(
                'slider_title' => $this->input->post('slider_title'),
                'slider_content' => $this->input->post('slider_content'),
                'ftitle' => $this->input->post('ftitle'),
                'fcontent' => $this->input->post('fcontent'),
                'stitle' => $this->input->post('stitle'),
                'scontent' => $this->input->post('scontent'),
                'thtitle' => $this->input->post('thtitle'),
                'thcontent' => $this->input->post('thcontent'),
                'slug' => $slug_n
            );

            if ($this->input->post('fourtitle') || $this->input->post('fourcontent')) {
                $data['fourtitle'] = $this->input->post('fourtitle');
                $data['fourcontent'] = $this->input->post('fourcontent');
            }
            
            if ($this->input->post('slug')) {
                if ($this->db->update('manage_services', $data, array('slug' => $slug_n))) {
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/manage_services/' . $slug_n);
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/manage_services/' . $slug_n);
                }
            }
        }

        $this->data['result'] = $this->db->get_where('manage_services', array('id' => $slug_n))->row();
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');
        $this->load->view('admin/manage_services', $this->data);
    }

    
    public function team($id = false) {

        $this->data['active'] = "team";
        if ($this->input->post()) {
            $data = array(
                'slider_title' => $this->input->post('slider_title'),
                'slider_content' => $this->input->post('slider_content'),
                'ftitle' => $this->input->post('ftitle'),
                'fcontent' => $this->input->post('fcontent'),
            );
            if ($this->input->post('id')) {
                if ($this->db->update('team', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/team/');
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/team/');
                }
            }
        }


        $this->data['result'] = $this->db->get_where('team', array('id' => 1))->row();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/team', $this->data);
    }
    
    public function manage_team($id = false) {

        $this->data['active'] = "team";

        if ($this->input->post()) {

            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {
                $new_name = time() . str_replace(str_split(' ()\\/,:*?"<>|'), '', $_FILES['file']['name']);
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = '*';
                $config['max_filename'] = '255';
                $config['file_name'] = $new_name;

                if (0 < $_FILES['file']['error']) {
                    // $response = array('status'=>301,'message'=>"Error during file upload");
                } else {
                    if (file_exists('upload/' . $new_name)) {
                        /// $response = array('status'=>301,'message'=>"File already exists");
                    } else {
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('file')) {
                            $this->upload->display_errors();
                        } else {
                            $fileName = $new_name;
                        }
                    }
                }
            }

            $data = array(
                'name' => $this->input->post('name'),
                'designation' => $this->input->post('designation'),
                'linkedin' => $this->input->post('linkedin'),
                'status' => $this->input->post('status'),
                'create_date' => date('Y-m-d h:i:s')
            );
            if ($fileName) {
                $data['file'] = $fileName;
            }

            if ($this->input->post('id')) {
                if ($this->db->update('manage_team_data', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/manage_team');
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/manage_team');
                }
            } else {
                if ($this->db->insert('manage_team_data', $data)) {
                    $this->session->set_flashdata('message', 'Data added successfully');
                    redirect('dashboard/manage_team');
                } else {
                    $this->session->set_flashdata('message', 'Data added unsuccessfully');
                    redirect('dashboard/manage_team');
                }
            }
        }

        if ($id) {
            $this->data['result'] = $this->db->get_where('manage_team_data', array('id' => $id))->row();
        }

        $this->data['section_data'] = $this->db->get('manage_team_data')->result();
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/manage_team', $this->data);
    }
    function delete_team($id) {
        $this->db->where('id', $id);
        $this->db->delete('manage_team_data');

        $this->session->set_flashdata('message', 'Record deleted successfully');
        redirect('dashboard/manage_team');
    }
    
    public function contact($id = false) {

        $this->data['active'] = "contact";
        if ($this->input->post()) {
            $data = array(
                'slider_title' => $this->input->post('slider_title'),
                'slider_content' => $this->input->post('slider_content'),
                'ftitle' => $this->input->post('ftitle'),
                'fcontent' => $this->input->post('fcontent'),
            );
            if ($this->input->post('id')) {
                if ($this->db->update('contact_content', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/contact/');
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/contact/');
                }
            }
        }

        $this->data['result'] = $this->db->get_where('contact_content', array('id' => 1))->row();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/contact', $this->data);
    }
    
    public function manage_contact_detail($id = false) {

        $this->data['active'] = "manage_contact_detail";

        if ($this->input->post()) {

            $data = array(
                'country_name' => $this->input->post('country_name'),
                'address' => $this->input->post('address'),
                'mobile_number' => $this->input->post('mobile_number'),
                'email' => $this->input->post('email'),
                'status' => $this->input->post('status')
            );
         
            if ($this->input->post('id')) {
                if ($this->db->update('manage_contact_detail', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Data updated successfully');
                    redirect('dashboard/manage_contact_detail');
                } else {
                    $this->session->set_flashdata('error', 'Data updated successfully');
                    redirect('dashboard/manage_contact_detail');
                }
            } else {
                if ($this->db->insert('manage_contact_detail', $data)) {
                    $this->session->set_flashdata('message', 'Data added successfully');
                    redirect('dashboard/manage_contact_detail');
                } else {
                    $this->session->set_flashdata('message', 'Data added unsuccessfully');
                    redirect('dashboard/manage_contact_detail');
                }
            }
        }

        if ($id) {
            $this->data['result'] = $this->db->get_where('manage_contact_detail', array('id' => $id))->row();
        }

        $this->data['section_data'] = $this->db->get('manage_contact_detail')->result();
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/manage_contact_detail', $this->data);
    }
    
    function delete_contact_detail($id){
        $this->db->where('id', $id);
        $this->db->delete('manage_contact_detail');

        $this->session->set_flashdata('message', 'Record deleted successfully');
        redirect('dashboard/manage_contact_detail');
    }
    
    public function case_studies($id = false) {

        $this->data['active'] = "case_studies";

        if ($this->input->post()) {

            $data = array(
                'title' => $this->input->post('title'),
                'content' => $this->input->post('content'),
                'status' => $this->input->post('status'),
                'create_date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('case_studies', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Case studies updated successfully');
                    redirect('dashboard/case_studies');
                } else {
                    $this->session->set_flashdata('error', 'Case studies updated successfully');
                    redirect('dashboard/case_studies');
                }
            } else {
                if ($this->db->insert('case_studies', $data)) {
                    $this->session->set_flashdata('message', 'Case studies added successfully');
                    redirect('dashboard/case_studies');
                } else {
                    $this->session->set_flashdata('message', 'Case studies added unsuccessfully');
                    redirect('dashboard/case_studies');
                }
            }
        }

       
        $this->db->order_by('id', 'desc');
        $this->db->where('id', $id);
        $this->data['res'] = $this->db->get('case_studies')->row();
     

        $this->db->order_by('id', 'desc');
        $this->data['result'] = $this->db->get('case_studies')->result();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/case_studies', $this->data);
    }
    
    
    function delete_case_studies($id) {
        $this->db->where('id', $id);
        $this->db->delete('case_studies');

        $this->session->set_flashdata('message', 'Record deleted successfully');
        redirect('dashboard/case_studies');
    }
    
    public function case_studies_sub_cat($caseid = false, $id = false) {

        $this->data['active'] = "case_studies_sub_cat";

        if ($this->input->post()) {
            $caseid = $this->input->post('caseid');
            $data = array(
                'case_study_id' => $caseid,
                'title' => $this->input->post('title'),
                'short_description' => $this->input->post('short_description'),
                'content' => $this->input->post('content'),
                'status' => $this->input->post('status'),
                'create_date' => date('Y-m-d h:i:s')
            );

            if ($this->input->post('id')) {
                if ($this->db->update('case_studies_sub_cat', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Case studies updated successfully');
                    redirect('dashboard/case_studies_sub_cat/'.$caseid);
                } else {
                    $this->session->set_flashdata('error', 'Case studies updated successfully');
                    redirect('dashboard/case_studies_sub_cat/'.$caseid);
                }
            } else {
                if ($this->db->insert('case_studies_sub_cat', $data)) {
                    $this->session->set_flashdata('message', 'Case studies added successfully');
                    redirect('dashboard/case_studies_sub_cat/'.$caseid);
                } else {
                    $this->session->set_flashdata('message', 'Case studies added unsuccessfully');
                    redirect('dashboard/case_studies_sub_cat/'.$caseid);
                }
            }
        }

       if($id){
        $this->db->order_by('id', 'desc');
        $this->db->where('id', $id);
        $this->data['res'] = $this->db->get('case_studies_sub_cat')->row();
       }

        $this->db->order_by('id', 'desc');
        $this->db->where('case_study_id',$caseid);
        $this->data['result'] = $this->db->get('case_studies_sub_cat')->result();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');
        $this->data['caseid']  = $caseid;
        $this->load->view('admin/case_studies_sub_cat', $this->data);
    }
    
    
    function delete_case_studies_sub_cat($caseid,$id) {
        $this->db->where('id', $id);
        $this->db->delete('case_studies_sub_cat');

        $this->session->set_flashdata('message', 'Record deleted successfully');
        redirect('dashboard/case_studies_sub_cat/'.$caseid);
    }
    
    function change_password(){
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');
        $this->load->view('admin/change_password', $this->data);
    }
    
    public function carousel_detail($id = false) {

        $this->data['active'] = "carousel_detail";

        if ($this->input->post()) {

            $data = array(
                'designation' => $this->input->post('designation'),
                'location' => $this->input->post('location'),
                'currency_symbol' => $this->input->post('currency_symbol'),
                'amount' => $this->input->post('amount'),
                'status' => $this->input->post('status'),
            );

            if ($this->input->post('id')) {
                if ($this->db->update('carousel_data', $data, array('id' => $this->input->post('id')))) {
                    $this->session->set_flashdata('message', 'Carousel updated successfully');
                    redirect('dashboard/carousel_detail');
                } else {
                    $this->session->set_flashdata('error', 'Carousel updated successfully');
                    redirect('dashboard/carousel_detail');
                }
            } else {
                if ($this->db->insert('carousel_data', $data)) {
                    $this->session->set_flashdata('message', 'Carousel added successfully');
                    redirect('dashboard/carousel_detail');
                } else {
                    $this->session->set_flashdata('message', 'Carousel added unsuccessfully');
                    redirect('dashboard/carousel_detail');
                }
            }
        }

        if($id){
            $this->db->order_by('id', 'desc');
            $this->db->where('id', $id);
            $this->data['res'] = $this->db->get('carousel_data')->row();
        }

        $this->db->order_by('id', 'desc');
        $this->data['result'] = $this->db->get('carousel_data')->result();

        $this->data['message'] = $this->session->flashdata('message');
        $this->data['error'] = $this->session->flashdata('error');

        $this->load->view('admin/carousel_detail', $this->data);
    }
    
     function delete_carousel_detail($id){
        $this->db->where('id', $id);
        $this->db->delete('carousel_data');

        $this->session->set_flashdata('message', 'Record deleted successfully');
        redirect('dashboard/carousel_detail');
    }
}
