<div role="main" class="main">
    <section id="start" class="section service_database bg-light-5 p-0">
        <div class="hreo_bred">

            <div class="container-fluid h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">

                        <div class="herroinner text-right">
                            <h3><?= $result->slider_title ?></h3>
                            <h4><?= html_entity_decode($result->slider_content) ?></h4>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <section id="start" class="section diviSection">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-9">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">
                                    <span class="top-sub-title">Services</span>
                                    <h2 class="font-weight-bold text-4 mb-3"><?= $result->ftitle ?></h2>
                                    <p><?= $result->fcontent ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg_serve3 permBg_match">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row ">
                            <div class="col-md-9">

                                <div class="herroinner">
                                    <h3><?= $result->stitle ?></h3>
                                    <h4><?= $result->scontent ?></h4>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg_serve4 permBg_match1">
            <div class="container-fluid h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row justify-content-end">
                            <div class="col-md-9">

                                <div class="herroinner text-right">
                                    <h3><?= $result->thtitle ?></h3>
                                    <h4 class="black_color"><?= $result->thcontent; ?></h4>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg2">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row ">
                            <div class="col-md-12">

                                <div class="herroinner text-right">
                                    <h3>What's next?</h3>
                                    <h4>Contact Us Today</h4>
                                     <p class="details"><strong>United Kingdom</strong> - +44 (0) 1962 677013 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details"><strong>Netherlands</strong> - +31 (0) 20 854 6378 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details">info@netrecruit.com <i class="fas fa-envelope"></i></p>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


</div>