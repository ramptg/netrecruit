
<!DOCTYPE html>
<html lang="zxx">
    <head>

        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Nework Recruit</title>	

        <meta name="keywords" content="" />
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,300,400,500,600,700,900%7COpen+Sans:300,400,600,700,800" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/front/css/bootstrap.min.css">

        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/front/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/front/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/front/css/magnific-popup.min.css">
        <script src="https://kit.fontawesome.com/10c3ba1463.js" crossorigin="anonymous"></script>


        <!-- Theme CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/front/css/theme.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/front/css/theme-elements.css">

        <!-- Current Page CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/front/css/settings.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/front/css/layers.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/front/css/navigation.css">

        <!-- Skin CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/front/css/default.css">	

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/front/css/custom.css">

        <!-- Head Libs -->
        <script src="<?php echo base_url() ?>assets/themes/front/css/modernizr.min.js"></script>

    </head>
    <body>
        <div class="body">

            <header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 120}">
                <div class="header-body">
                    <div class="row justify-content-center">
                        <div class="col-md-10">

                            <div class="header-container container-fluid">

                                <div class="header-row">
                                    <div class="header-column justify-content-start">
                                        <div class="header-logo">
                                            <a href="<?php echo base_url(); ?>">
                                                <img alt="EZ" width="197" height="" src="<?php echo base_url() ?>assets/themes/front/images/network-executive-black.png">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="header-column justify-content-end">
                                        <div class="header-nav">
                                            <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                                                <nav class="collapse">
                                                    <ul class="nav flex-column flex-lg-row" id="mainNav">
                                                        <li class="">
                                                            <a class=" active" href="<?php echo base_url(); ?>">
                                                                About
                                                            </a>

                                                        </li>
                                                        <li class="dropdown">
                                                            <a class="dropdown-item dropdown-toggle" href="#">
                                                                Divisions
                                                            </a>
                                                            <ul class="dropdown-menu">
                                                                <li class=""><a class="" href="division-executive">Network Executive </a></li>
                                                                <li class=""><a class="" href="division-professional">Network Professional</a></li>
                                                                <li class=""><a class="" href="division-consultancy">Network Consultancy</a></li>



                                                            </ul>
                                                        </li>
                                                        <li class="dropdown">
                                                            <a class="dropdown-item dropdown-toggle" href="#">
                                                                Services
                                                            </a>
                                                            <ul class="dropdown-menu">
<!--                                                                <li class=""><a class="" href="services-tachnology">Direct Hiring</a></li>-->
                                                                <li class=""><a class="" href="search">Search</a></li>
                                                                <li class=""><a class="" href="services-advertised">Advertised Selection</a></li>
                                                                <li class=""><a class="" href="services-database-search">Database Search</a></li>

                                                                <!-- 			<li class=""><a class="" href="services-consulting.html">Consulting</a></li> -->



                                                            </ul>

                                                        </li>
                                                        <li class="">
                                                            <a class="" href="team">
                                                                Team
                                                            </a>

                                                        </li>
                                                        <li class="">
                                                            <a class="" href="contactus">
                                                                Contact US
                                                            </a>

                                                        </li>

                                                    </ul>
                                                </nav>
                                            </div>

                                            <button class="header-btn-collapse-nav ml-3" data-toggle="collapse" data-target=".header-nav-main nav">
                                                <span class="hamburguer">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </span>
                                                <span class="close">
                                                    <span></span>
                                                    <span></span>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <?php echo $output; ?> 

            <footer id="footer" class="footer-hover-links-light mt-0">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-lg-10 mb-4 mb-lg-0">
                            <div class="row justify-content-center">
                                <div class="col-lg-2 mb-4 mb-lg-0">
                                    <a href="<?php echo base_url(); ?>" class="logo">
                                        <img alt="EZY Website Template" class=" mb-3" src="<?php echo base_url() ?>assets/themes/front/images/logo-footer.png">
                                    </a>
                                    <ul class="social-icons social-icons-transparent social-icons-icon-light social-icons-lg">									<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>									<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>									<li class="social-icons-instagram"><a href="http://www.instagram.com/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>								</ul>	

                                </div>
                                <div class="col-lg-2 mb-4 mb-lg-0">

                                    <ul class="list list-icon list-unstyled">
                                        <li class="mb-2"><a href="<?php echo base_url(); ?>">About Us</a></li>

                                    </ul>
                                </div>
                                <div class="col-lg-2 mb-4 mb-lg-0">

                                    <ul class="list list-icon list-unstyled">
                                        <li class="mb-2"><a href="division-executive">Executive</a></li>
                                        <li class="mb-2"><a href="division-professional">Professional</a></li>
                                        <li class="mb-2"><a href="division-direct">Consultancy</a></li>

                                    </ul>
                                </div>
                                <div class="col-lg-2 mb-4 mb-lg-0">

                                    <ul class="list list-icon list-unstyled">
                                        <li class="mb-2"><a href="services-tachnology">Direct Hiring</a></li>
                                        <li class="mb-2"><a href="search">Search</a></li>
                                        <li class="mb-2"><a href="services-advertised">Advertised Selection</a></li>
                                        <li class="mb-2"><a href="services-database-search">Database Search</a></li>


                                    </ul>
                                </div>
                                <div class="col-lg-2 mb-4 mb-lg-0">

                                    <ul class="list list-icon list-unstyled">
                                        <li class="mb-2"><a href="team">Team</a></li>

                                    </ul>
                                </div>
                                <div class="col-lg-2 mb-4 mb-lg-0">

                                    <ul class="list list-icon list-unstyled">
                                        <li class="mb-2"><a href="contactus">Contact Us</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div></div>
                </div>


                <div class="footer-copyright1 text-center">				
                    <div class="container">
                        <a target="_blank" href="<?php echo base_url('terms-and-conditions'); ?>">Terms & Conditions </a>    |    <a  target="_blank" href="<?php echo base_url('privacy-policy'); ?>"> Privacy Policy </a>       |     <a  target="_blank" href="<?php echo base_url('cookie-ploicy'); ?>">Cookie Policy </a>       |    <a target="_blank" href="<?php echo base_url('data-retention'); ?>"> Data Retention </a>       |     <a target="_blank" href="https://networkexecutive.com/blog/"> Blog</a>   				</div>				</div>
                <div class="footer-copyright">				
                    <div class="container">
                        <div class="row text-center text-md-left align-items-center font-16">	
                            <div class="col-md-6 col-lg-6 font-weight-bold">Copyright &COPY; 2020 Network Executive. All rights reserved.</div>
                            <div class="col-md-6 col-lg-6">							
                                <p class="text-md-right pb-0 mb-0">Designed by Agitate Digital. Developed & Maintained by <a href="http://www.protechgenie.com/" target="_blank">ProtechGenie</a>.</p>							</div>						</div>					</div>				</div>

            </footer>
        </div>

        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.appear.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.easing/jquery.easing.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.cookie.js"></script>
        <!-- 	<script src="js/style.switcher.js" id="styleSwitcherScript" data-base-path="" data-skin-src=""></script> -->
        <script src="<?php echo base_url() ?>assets/themes/front/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/common.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.easypiechart.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.gmap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.lazyload.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.isotope.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/owl.carousel.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.vide.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/vivus.min.js"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="<?php echo base_url() ?>assets/themes/front/js/theme.js"></script>

        <!-- Current Page Vendor and Views -->
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.themepunch.tools.min.js"></script>		
        <script src="<?php echo base_url() ?>assets/themes/front/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Theme Custom -->
        <script src="<?php echo base_url() ?>assets/themes/front/js/custom.js"></script>

        <!-- Theme Initialization Files -->
        <script async src="<?php echo base_url() ?>assets/themes/front/js/theme.init.js"></script>
        <script type="text/javascript">

            var revapi = jQuery(document).ready(function () {

                jQuery('#rev_slider_1').show().revolution({

                });
            });

            revapi.on('revolution.slide.onvideoplay', function (event, data) {

                // data.video = The Video API to Manage Video functions
                // data.videotype = youtube, vimeo, html5
                // data.settings = Video Settings

                switch (data.videotype) {

                    case 'html5':

                        // data.video = YouTube iframe API reference:
                        // https://developers.google.com/youtube/iframe_api_reference#Playback_controls

                        break;

                    case 'vimeo':

                        // data.video = Vimeo iframe API reference
                        // https://github.com/vimeo/player.js

                        break;

                    case 'youtube':
                        console.log(data);
                        vid = data.video;

                        vid.ontimeupdate = function () {
                            $('.cap1').hide();
                            $('.cap2').hide();
                            $('.cap3').hide();
                            $('.cap4').hide();
                            $('.cap5').hide();
                            $('.cap6').hide();
                            myFunction()
                        };

                        function myFunction() {
                            // Display the current position of the video in a <p> element with id="demo"


                            if ((vid.currentTime >= 0) && (vid.currentTime <= 8)) {
                                $('.cap1').show();
                            } else if ((vid.currentTime >= 9) && (vid.currentTime <= 15)) {
                                $('.cap2').show();
                            } else if ((vid.currentTime >= 16) && (vid.currentTime <= 21)) {
                                $('.cap3').show();
                            } else if ((vid.currentTime >= 22) && (vid.currentTime <= 27)) {
                                $('.cap4').show();
                            } else if ((vid.currentTime >= 28) && (vid.currentTime <= 38)) {
                                $('.cap5').show();
                            } else if ((vid.currentTime >= 41) && (vid.currentTime <= 44)) {
                                $('.cap6').show();
                            }
                        }
                        // data.video = HTML Video element:
                        // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video

                        break;
                }

            });
        </script>
        <script src="<?php echo base_url() ?>assets/themes/front/js/examples.portfolio.js"></script>
    </body>
</html>