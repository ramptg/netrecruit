<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Network Executive</title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/iconfonts/ionicons/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/iconfonts/typicons/src/font/typicons.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/css/vendor.bundle.base.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/css/vendor.bundle.addons.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
        <!-- endinject -->
        <!-- plugin css for this page -->
        <!-- End plugin css for this page -->

        <!-- inject:css -->
        <script src="<?php echo base_url() ?>assets/themes/admin/vendors/js/vendor.bundle.base.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/admin/vendors/js/vendor.bundle.addons.js"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/css/shared/style.css">
        <!-- endinject -->
        <!-- Layout styles -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/css/demo_1/style.css">
        <!-- End Layout styles -->
        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/themes/admin/images/favicon.png" />
        <script src="<?php echo base_url() ?>assets/themes/admin/js/jquery.min.js"></script>
 
 <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.js"></script>
    </head>
    <body>
        <div class="container-scroller">
            <!-- partial:partials/_navbar.html -->
            <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
                    <a class="navbar-brand brand-logo" href="<?php echo base_url('dashboard'); ?>">
                        <img src="<?php echo base_url() ?>assets/themes/admin/images/network-executive.png" alt="logo" /> </a>
                    <a class="navbar-brand brand-logo-mini" href="index.html">
                        <img src="<?php echo base_url() ?>assets/themes/admin/images/mini-network-executive.png" alt="logo" /> </a>
                </div>
                <div class="navbar-menu-wrapper d-flex align-items-center">

                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item  d-xl-inline-block ">

                            <a class="nav-link" id="UserDropdown" href="<?php echo base_url('auth/logout') ?>" aria-expanded="false">
                                Logout <i class="mdi mdi-logout"></i> </a>

                        </li>
                    </ul>
                    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                        <span class="mdi mdi-menu" style="color: #fff"></span>
                    </button>
                </div>
            </nav>
            <!-- partial -->
            <div class="container-fluid page-body-wrapper">
                <!-- partial:partials/_sidebar.html -->
                <nav class="sidebar sidebar-offcanvas" id="sidebar">
                    <ul class="nav">

                        <li class="nav-item nav-category">Main Menu</li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('dashboard') ?>">
                                <i class="menu-icon typcn typcn-document-text"></i>
                                <span class="menu-title">Dashboard</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('dashboard/case_studies') ?>">
                                <i class="menu-icon typcn typcn-document-text"></i>
                                <span class="menu-title">Manage Case Studies</span>
                            </a>
                        </li>
                        
                         <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('dashboard/carousel_detail') ?>">
                                <i class="menu-icon typcn typcn-document-text"></i>
                                <span class="menu-title">Manage Carousel</span>
                            </a>
                        </li>
                        
                          <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('dashboard/change_password') ?>">
                                <i class="menu-icon typcn typcn-document-text"></i>
                                <span class="menu-title">Change Password</span>
                            </a>
                        </li>
                       <!-- <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('dashboard/slider_content') ?>">
                                <i class="menu-icon typcn typcn-document-text"></i>
                                <span class="menu-title">Manage Slider Section</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#about" aria-expanded="false" aria-controls="ui-basic">
                                <i class="menu-icon typcn typcn-coffee"></i>
                                <span class="menu-title">Manage About us</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="about">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_aboutus') ?>">Manage Aboutus Content</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/aboutus_section') ?>">Manage Aboutus Section</a>
                                    </li>

                                </ul>
                            </div>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#nedivisions" aria-expanded="false" aria-controls="ui-basic">
                                <i class="menu-icon typcn typcn-coffee"></i>
                                <span class="menu-title">Network Executive Divisions</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="nedivisions">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_network_division/1') ?>">Manage Network Division</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/exe_multi_fun_expertise') ?>">Multi-functional Expertise</a>
                                    </li>

                                </ul>
                            </div>
                        </li>
                        
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#pddivisions" aria-expanded="false" aria-controls="ui-basic">
                                <i class="menu-icon typcn typcn-coffee"></i>
                                <span class="menu-title">Network Professional Divisions</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="pddivisions">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_network_division/2') ?>">Manage Professional Division</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/pro_multi_fun_expertise') ?>">Multi-functional Expertise</a>
                                    </li>

                                </ul>
                            </div>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#cdivisions" aria-expanded="false" aria-controls="ui-basic">
                                <i class="menu-icon typcn typcn-coffee"></i>
                                <span class="menu-title">Network Consultancy Divisions</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="cdivisions">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_network_division/3') ?>">Manage Consultancy Division</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/con_multi_fun_expertise') ?>">Direct Hiring Solutions</a>
                                    </li>

                                </ul>
                            </div>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#services" aria-expanded="false" aria-controls="ui-basic">
                                <i class="menu-icon typcn typcn-coffee"></i>
                                <span class="menu-title">Services</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="services">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_services/1') ?>">Direct Hiring</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_services/2') ?>">Search</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_services/3') ?>">Advertised Selection</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_services/4') ?>">Database Search</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#teams" aria-expanded="false" aria-controls="ui-basic">
                                <i class="menu-icon typcn typcn-coffee"></i>
                                <span class="menu-title">Team</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="teams">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/team') ?>">Manage Team Content</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_team') ?>">Manage Team</a>
                                    </li>

                                </ul>
                            </div>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#Contact" aria-expanded="false" aria-controls="ui-basic">
                                <i class="menu-icon typcn typcn-coffee"></i>
                                <span class="menu-title">Contact</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="Contact">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/contact') ?>">Manage Contact Content</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_contact_detail') ?>">Manage Contact Detail</a>
                                    </li>

                                </ul>
                            </div>
                        </li>-->
                        <!--
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#ui-basic_80" aria-expanded="false" aria-controls="ui-basic">
                                <i class="menu-icon typcn typcn-coffee"></i>
                                <span class="menu-title">Manage (How We Do It)</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="ui-basic_80">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_howwedoit') ?>">Manage Content</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/section_howwedoit') ?>">Manage Section</a>
                                    </li>

                                </ul>
                            </div>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#ui-basic_90" aria-expanded="false" aria-controls="ui-basic">
                                <i class="menu-icon typcn typcn-coffee"></i>
                                <span class="menu-title">Manage (Who Does It)</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="ui-basic_90">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_whodoesit') ?>">Manage Content</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/manage_team') ?>">Senior Management Team</a>
                                    </li>

                                </ul>
                            </div>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('dashboard/join_the_team') ?>">
                                <i class="menu-icon typcn typcn-document-text"></i>
                                <span class="menu-title">Join The Team</span>
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url('dashboard/lets_talk_content') ?>">
                                <i class="menu-icon typcn typcn-document-text"></i>
                                <span class="menu-title">Manage (Let's Talk)</span>
                            </a>
                        </li>
                        -->
                        <!-- ram<li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#ui-basic_70" aria-expanded="false" aria-controls="ui-basic">
                                <i class="menu-icon typcn typcn-coffee"></i>
                                <span class="menu-title">Pages</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="ui-basic_70">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/pages/1') ?>">Terms & Conditions</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/pages/2') ?>">Privacy Policy</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/pages/3') ?>">Cookie Policy</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('dashboard/pages/4') ?>">Data Retention</a>
                                    </li>
                                </ul>
                            </div>
                        </li>-->
                        
                            <!-- <li class="nav-item">
                            <a class="nav-link" href="<?php //echo base_url('dashboard/contact_details') ?>">
                                <i class="menu-icon typcn typcn-document-text"></i>
                                <span class="menu-title">Contact Details</span>
                            </a>
                        </li>-->

                    </ul>
                </nav>
                <!-- partial -->

                <div class="main-panel"> 
                    <?php echo $output; ?>
                    <footer class="footer">
                        <div class="container-fluid clearfix">
                            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright &copy; <?php echo date('Y'); ?> <a href="http://www.protechgenie.com/" target="_blank">ProTechGenie</a>. All rights reserved.</span>

                        </div>
                    </footer>
                    <!-- partial -->
                </div>
                <!-- main-panel ends -->
            </div>
            <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->
        <!-- plugins:js -->
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/admin/js/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/themes/admin/js/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>assets/themes/admin/js/selectsearch.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>

        <script>
            $('#summernote').summernote({
                placeholder: '',
                tabsize: 2,
                height: 150,

            });
            $('#summernote_2').summernote({
                placeholder: '',
                tabsize: 2,
                height: 500,

            });
            
            $('#summernote_3').summernote({
                placeholder: '',
                tabsize: 2,
                height: 150,

            });
            $('#summernote_4').summernote({
                placeholder: '',
                tabsize: 2,
                height: 100,

            });
             $('#summernote_5').summernote({
                placeholder: '',
                tabsize: 2,
                height: 100,

            });
        </script>

        <script>
            $("#form_data_submit").validate({
                rules: {
                    password: "required",
                    confirmpassword: {
                        equalTo: "#password"
                    }
                },
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input

                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                success: function (label, element) {
                    label.hide();
                    $(element).closest('.form-group').removeClass('has-error');
                },

                errorPlacement: function (error, element) {
                    error.insertAfter(element.closest('.form-group'));
                },

                submitHandler: function (form, e) {
                    submit.form();
                }
            });
        </script>
        <script>

            $('table').dataTable({bFilter: true, bInfo: false});
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>



        <!-- endinject -->
        <!-- Plugin js for this page-->
        <!-- End plugin js for this page-->
        <!-- inject:js -->

        <script src="<?php echo base_url() ?>assets/themes/admin/js/shared/off-canvas.js"></script>
        <script src="<?php echo base_url() ?>assets/themes/admin/js/shared/misc.js"></script>
        <!-- endinject -->
        <!-- Custom js for this page-->
        <script src="<?php echo base_url() ?>assets/themes/admin/js/demo_1/dashboard.js"></script>
        <!-- End custom js for this page-->
    </body>
</html>