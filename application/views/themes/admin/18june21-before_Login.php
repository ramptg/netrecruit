<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Network Executive</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/iconfonts/ionicons/css/ionicons.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/iconfonts/typicons/src/font/typicons.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/css/vendor.bundle.addons.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/css/shared/style.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/iconfonts/font-awesome/css/font-awesome.min.css" />
    <!-- endinject -->
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/themes/admin/images/favicon.png" />
    <style type="text/css">
        .permBg2 {
    background: url(../images/division_bg2.png) no-repeat center;
    
    position: relative;
}.permBg2:before {
    content: "";
    position: absolute;
    height: 100%;
    width: 100%;
    display: block;
    top: 0;
    right: 0;
    z-index: 15;
    background: linear-gradient(to left,rgba(23,34,54,1) 20%,rgba(23,34,54,.95) 40%,rgba(23,34,54,0.3) 80%,rgba(23,34,54,1) 100%);
    opacity: 0.9;
}
    </style>
  </head>
  <body>

<div class="permBg2">
  	<div class="app">
        <div class="container-fluid">
            <div class="d-flex full-height p-v-15 flex-column justify-content-between">
                <div class="d-none d-md-block p-h-40">
                   
                </div>
                <div class="container">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-5">
                            <div class="card">

                                <div class="card-body"><div class="m-t-20 text-center"> <img width="200" src="<?php echo base_url() ?>assets/themes/admin/images/network-executive.png" alt=""></div>
                                    <h2 class="m-t-20">Sign In</h2>
                                    <p class="m-b-30" style="color: #fff">Enter your credential to get access</p>


                                     <?php echo $output; ?> 
                              
                                </div>
                            </div>
                        </div>
                       <!--  <div class="offset-md-1 col-md-6 d-none d-md-block">
                            <img class="img-fluid" src="<?php echo base_url() ?>assets/themes/admin/images/student1.png" alt="">
                        </div> -->
                    </div>
                </div>
                <div class="d-none d-md-block p-h-40 justify-content-between text-center">
                    <span class="">© <?php echo date('Y');?> Network Executive</span>
                    <!-- <ul class="list-inline">
                        <li class="list-inline-item">
                            <a class="text-dark text-link" href="">Legal</a>
                        </li>
                        <li class="list-inline-item">
                            <a class="text-dark text-link" href="">Privacy</a>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>

 <!--    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
            
              <ul class="auth-footer">
             
              </ul>
             
            </div>
          </div>
        </div>
        content-wrapper ends
      </div>
      page-body-wrapper ends 
    </div> -->
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?php echo base_url() ?>assets/themes/admin/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/themes/admin/js/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/themes/admin/js/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/themes/admin/js/jquery.login.js"></script>

    <script src="<?php echo base_url() ?>assets/themes/admin/vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- inject:js -->
  
    <script src="<?php echo base_url() ?>assets/themes/admin/js/shared/misc.js"></script>
    <!-- endinject -->
       <script>
            jQuery(document).ready(function () {
                Login_User.init();
            });
        </script>
  </body>
</html>