<form method="post" id="login_user" action="<?php echo base_url('auth/login') ?>">

    <div class="login_error_message alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        You have some form errors. Please check below.
    </div>
    <div class="alert alert-success display-hide">
        <button class="close" data-close="alert"></button>
        Your form validation is successful!
    </div>

    <div class="form-group">
        <label class="font-weight-semibold" for="userName">Username</label>
        <div class="input-affix">
            <i class="prefix-icon fa fa-user-o"></i>
            <input name="identity" type="text" class="form-control" placeholder="Username">
        </div>
    </div>
    <div class="form-group">
        <label class="font-weight-semibold" for="password">Password</label>

        <div class="input-affix m-b-10">
            <i class="prefix-icon fa fa-lock"></i>
            <input name="password" type="password" class="form-control" placeholder="*********">
        </div>
    </div>
    <div class="form-group">
        <div class="d-block align-items-center text-center">

            <button type="submit" class="btn btn-primary ">Login <img src="<?php echo base_url() ?>assets/themes/admin/images/lazyLoadImage.gif" id="loader2" style="font-size:24px;width: 31px; left: 90px; top: 44px;" class="loaders display-hide"></button>

        </div>
    </div>
</form>




<!-- <div class="auto-form-wrapper">
    <form method="post" id="login_user" action="<?php //echo base_url('auth/login')  ?>">
       
        <div class="form-group">
            <label class="label">Username</label>
            <div class="input-group">
                
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="label">Password</label>
            <div class="input-group">
               
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="mdi mdi-check-circle-outline"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            
        </div>

        <div class="text-block text-center my-3"> </div>
    </form>
</div>

-->