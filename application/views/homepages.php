
<div class="theme-layout" id="scrollup">
    <header class="stick-top style2">
        <div class="menu-sec">
            <div class="container ">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="logo1">
                            <a href="<?php echo base_url(); ?>" title=""><img src="<?php echo base_url() ?>assets/themes/front/images/network-executive.png" alt="<strong>Network Executive</strong>" /></a>
                        </div><!-- Logo -->
                    </div>
                </div>

            </div>
        </div>
    </header>
    <div class="job-grid-sec1" style="padding: 140px 0" >
        <div class="container">
            <div class="" style="margin-bottom: 10px">
                <h2 class="text-center"><?php echo $pages->page_title; ?></h2><br>
                <p><strong></strong></p>
                <?php echo html_entity_decode($pages->text); ?>
            </div>
        </div>
    </div>
</div>



