<div role="main" class="main">
    <section id="start" class="section service_conslt contact_us bg-light-5 p-0">
        <div class="hreo_bred" style="height: 200px">

            <div class="container-fluid h-100" style="position: relative;">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">

                        <div class="herroinner text-right">
                            <h3>Data Retention</h3>


                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
    <section id="start" class="section diviSection">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">


                                    <p>
                                        <strong>1.1)</strong> This policy has been designed to help  ensure that we comply with our Legal obligations in relation to the retention  and deletion of personal data.<br>
                                        <strong>1.2)</strong> Personal data that we process for any  purpose or purposes shall not be kept for longer than is necessary for that  purpose or those purposes.<br>
                                        <strong>1.3)</strong> We will retain and delete your personal  data as follows:<br></p>
                                    <p> Account Data will be retained for 3 years  following the date of our last contact or dealing with you, at the end of which  period it will be deleted from our systems.</p>
                                    <p> Profile Data (other than Profile Data which  is also Account Data) will be retained for 3 years following the date of our  last contact or dealing with you, at the end of which period it will be deleted  from our systems.</p>
                                    <p>Contact Data (other than Contact Data which  is also Account Data) will be retained for 2 years following the date of our  last contact or dealing with you, at the end of which period it will be deleted  from our systems.</p>
                                    <p>Usage Data will be retained for 2 years  following the date of our last contact or dealing with you, at the end of which  period it will be deleted from our systems.<br>
                                        Enquiry Data (other than Enquiry Data which  is also Account Data) will be retained for 2 years following the date of our  last contact or dealing with you, at the end of which period it will be deleted  from our systems.</p>
                                    <p>Transaction Data will be retained for 2 years  following the date of our last contact or dealing with you, at the end of which  period it will be deleted from our systems.</p>
                                    <p>Notification Data (other than Notification  Data which is also Account Data) will be retained for 2 years following the  date of our last contact or dealing with you, at the end of which period it  will be deleted from our systems.</p>
                                    <p>Correspondence Data (other than  Correspondence Data which is also Account Data) will be retained for 2 years  following the date of our last contact or dealing with you, at the end of which  period it will be deleted from our systems.</p>
                                    <strong>1.4)</strong> Notwithstanding the other provisions of  this policy, we may retain your personal data where such retention is necessary  for compliance with a legal obligation to which we are subject, or in order to  protect your vital interests or the vital interests of another natural person.<p></p>
                                    <p><strong>2) Amendments</strong><br>
                                        2.1) We may update this policy from time to  time by publishing a new version on our website.<br>
                                        2.2) You should check this page occasionally  to ensure you are happy with any changes to this policy.<br>
                                        2.3) We may notify you of changes to this  policy by email.</p>
                                    <p><strong>3) Our Details</strong><br>
                                        3.1) This website is owned and operated by Network Executive Limited<br>
                                        <a name="_GoBack" id="_GoBack"></a>3.2) We are registered in England and Wales under Registration Number 07768390, VAT Number 324 8522 07.<br>
                                        Our Registered Office and principal place of business is at The Old Dairy, Western Court, Bishops Sutton, Alresford, Hampshire, SO24 0AA.<br>
                                        3.3) You can contact us:</p>
                                    <ul>
                                        <li>By post, using the postal  address given above</li>
                                        <li>Using our website contact  form</li>
                                        <li>By telephone, on the contact  number published on our website from time to time</li>
                                        <li>By email, using the email  address published on our website from time to time</li>
                                    </ul>




                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>




</div>