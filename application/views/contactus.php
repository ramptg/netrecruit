<div role="main" class="main">
    <section id="start" class="section service_conslt contact_us bg-light-5 p-0">
        <div class="hreo_bred">

            <div class="container-fluid h-100" style="position: relative;">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">

                        <div class="herroinner text-right">
                            <h3><?= html_entity_decode($result->slider_title); ?></h3>
                            <h4><?= html_entity_decode($result->slider_content); ?></h4>

                        </div>
                        <div class="conab">
                           
                            <h4>Contact us today</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="start" class="section diviSection">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">
                                    <span class="top-sub-title">Contact Us</span>
                                    <h2 class="font-weight-bold text-4 mb-3"><?= $result->ftitle; ?></h2>
                                    <?= html_entity_decode($result->fcontent); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
<!-- 
        <?php
    if ($contact) {
        foreach ($contact as $v) {
            if($v->id == 1){
                $class = "";
            }else{
                $class = "contactBg1";
            }
            ?>
            <section id="start" class="section  bg-light-5 p-0">
                <div class="hreo_bred  contactBg <?php echo $class; ?>">
                    <div class="container-fluid  h-100">
                        <div class="row justify-content-center align-items-center h-100">
                            <div class="col-md-8">
                                <div class="row ">

                                    <div class="col-md-12">
                                        <div class="herroinner1 text-left">
                                            <h3><?php echo $v->country_name ?></h3>
                                            <p class="headadd"><i class="fas fa-map-marker"></i>  <span><?php echo html_entity_decode($v->address) ?></span></p>
                                            <p class="details"><i class="fas fa-phone-alt"></i> <?php echo $v->mobile_number ?></p>
                                            <p class="details"><i class="fas fa-envelope"></i> <?php echo $v->email ?> </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    <?php }
} ?> -->
  <!--  <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred  contactBg">
            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row ">
                            <div class="col-md-12">

                                <div class="herroinner1 text-left">
                                    <h3>United Kingdom</h3>
                                    <p class="headadd"><i class="fas fa-map-marker"></i>  <span>The Old Dairy, Western Court, Bishops Sutton <br>Road, Alresford, Hampshire SO24 0AA</span></p>
                                    <p class="details"><i class="fas fa-phone-alt"></i> +44 (0) 1962 677013</p>
                                    <p class="details"><i class="fas fa-envelope"></i> info@networkexecutive.com </p>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred  contactBg contactBg1">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row ">
                            <div class="col-md-12">

                                <div class="herroinner1 text-left">
                                    <h3>Netherlands</h3>
                                    <p class="headadd"><i class="fas fa-map-marker"></i>  <span>Nieuwezijds Voorburgwal 104-108, 1012 SG Amsterdam</span></p>
                                    <p class="details"><i class="fas fa-phone-alt"></i> +31 20 854 6378</p>
                                    <p class="details"><i class="fas fa-envelope"></i> info@networkexecutive.com </p>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
-->
    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg2">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">
                        <div class="row ">
                            <div class="col-md-12">

                                <div class="herroinner text-right">
                                    <h3>What’s next?</h3>
                                    <h4>Contact Us Today</h4>
                                    <p class="details"><strong>United Kingdom</strong> - +44 (0) 1962 677013 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details"><strong>Netherlands</strong> - +31 (0) 20 854 6378 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details">info@netrecruit.com <i class="fas fa-envelope"></i></p>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


</div>