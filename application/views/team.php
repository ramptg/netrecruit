<div role="main" class="main">
    <section id="start" class="section service_conslt teram_our  bg-light-5 p-0">
        <div class="hreo_bred">

            <div class="container-fluid h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">

                        <div class="herroinner text-right">
                            <h3><?= $result->slider_title; ?></h3>
                            <h4><?= html_entity_decode($result->slider_content); ?></h4>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="start" class="section diviSection">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">
                                <div class="icon-box-info">
                                    <span class="top-sub-title">Our Team</span>
                                    <h2 class="font-weight-bold text-4 mb-3"><?= $result->ftitle; ?></h2>
                                    <?= html_entity_decode($result->fcontent); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <section id="start" class="section team_sec ">
        <div class=" ">

            <div class="container h-100">
                <h2 class="text-center">Our executive management team comprises of </h2><br><br>
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-12">
                        <div class="row ">
                            <?php
                            if ($team) {
                                foreach ($team as $v) {
                                    ?>
                                    <div class="col-lg">
                                        <div class="team_block">
                                            <div class="team_img">
                                                <?php if($v->file){ ?>
                                                <img class="img-fluid" src="<?php echo base_url('upload/'.$v->file) ?>">
                                                <?php }else{ ?>
                                                <img class="img-fluid" src="<?php echo base_url('assets/themes/front/images/team_img1.png') ?>">
                                                <?php } ?>
                                                <div class="fb_btn"><a href="<?php echo isset($v->linkedin) && !empty($v->linkedin) ? $v->linkedin : 'javascript::void(0);'?>"><img src="<?php echo base_url() ?>assets/themes/front/images/linkbtn.png"></a></div>
                                            </div>
                                            <div class="team_title">
                                                <h3><?php echo $v->name; ?></h3>
                                                <p><?php echo $v->designation; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                            } ?>
               
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>

    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg2">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row ">
                            <div class="col-md-12">

                                <div class="herroinner text-right">
                                    <h3>What's next?</h3>
                                    <h4>Contact Us Today</h4>
                                      <?php
                                        if ($contact) {
                                            foreach ($contact as $v) {
                                                ?>
                                                <p class="details"> <strong><?= $v->country_name; ?></strong> <?= $v->mobile_number; ?> <i class="fas fa-phone-alt"></i></p>
                                            <?php }
                                        } ?>
                                    <p class="details">info@netrecruit.com <i class="fas fa-envelope"></i></p>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


</div>