<div role="main" class="main">
    <section id="start" class="section bg-light-5 p-0">
        <div class="hreo_bred">

            <div class="container-fluid h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">
                        <div class="herroinner text-right">
                            <h3><img src="<?php echo base_url() ?>assets/themes/front/images/net-executive-white.png"></h3>
                            <h4><?= html_entity_decode($result->slider_content); ?></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section id="start" class="section diviSection">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">
                                    <span class="top-sub-title">Divisions</span>
                                    <!-- <h2 class="font-weight-bold text-4 mb-3"><?= $result->ftitle; ?></h2> -->
                                    <h2 class="font-weight-bold text-4 mb-3">Net Executive</h2>
                                    <!-- <p><?= $result->fcontent; ?></p> -->
                                    <p>Net Executive seamlessly supports complex, international organisations to hire the talent they need around the world.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row ">
                            <div class="col-md-9">

                                <div class="herroinner">
                                    <h3><?= $result->stitle; ?></h3>
                                    <h4><?= $result->scontent; ?> </h4> 

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg1 permBg03">
            <div class="container-fluid h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row justify-content-end">
                            <div class="col-md-9">

                                <div class="herroinner text-right">
                                    <h3><?= $result->thtitle; ?> </h3>
                                    <h4><?= $result->thcontent; ?></h4>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <div class="newstart">
        <div class="text-center"><h2>Multi-functional Expertise</h2></div>
        <div class="container">
            <div class="expert-blog">

                <div class="row">
                    <?php
                    if ($fresult) {
                        foreach ($fresult as $val) {
                            ?>
                            <div class="col-md-2dot4">
                                <div class="imgBLo">
                                    <img class="img-fluid" src="<?php echo base_url('upload/'.$val->file) ?>">
                                </div>
                                <div class="imgTextB">
                                    <?= $val->title; ?>
                                </div>
                            </div>    
                        <?php
                        }
                    }
                    ?>
                </div>
            </div>


        </div>	

    </div>	
    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg2">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row ">
                            <div class="col-md-12">

                                <div class="herroinner text-right">
                                    <h3>What's next?</h3>
                                    <h4>Contact Us Today</h4>
                                      <?php
                                        if ($contact) {
                                            foreach ($contact as $v) {
                                                ?>
                                              <p class="details"> <strong><?= $v->country_name; ?></strong> <?= $v->mobile_number; ?> <i class="fas fa-phone-alt"></i></p>
                                            <?php }
                                        } ?>
                                    <p class="details">info@netrecruit.com <i class="fas fa-envelope"></i></p>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


</div>