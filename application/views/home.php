<div role="main" class="main">
    <div class="slider-container  rev_slider_wrapper">
        <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'gridwidth': [1900,1140,960,720,540], 'gridheight': [600,350,350,800,800], 'disableProgressBar': 'on', 'responsiveLevels': [4096,1200,992,576], 'navigation' : {'arrows': { 'enable': false, 'hide_under': 767, 'style': 'slider-arrows-style-1' }, 'bullets': {'enable': false, 'style': 'bullets-style-1', 'h_align': 'center', 'v_align': 'bottom', 'space': 7, 'v_offset': 35, 'h_offset': 0}}}">
            <ul>
                <li data-transition="fade">
                    <img src="video/cover-vid.png"  
                         alt=""
                         data-bgposition="center center" 
                         data-bgfit="cover" 
                         data-bgrepeat="no-repeat" 
                         class="rev-slidebg">

                    <div class="rs-background-video-layer z-index-10" 
                         data-forcerewind="on" 
                         data-volume="mute" 
                         data-ytid="wFtejy6NyH0" 
                         data-videoattributes="version=3&amp;enablejsapi=1&amp;html5=1&amp;hd=1&amp;wmode=opaque&amp;showinfo=0&amp;rel=0;" 

                         data-videowidth="100%" 
                         data-videoheight="100%" 
                         data-videocontrols="none" 
                         data-videopreload="preload" 

                         data-videoloop="loop" 
                         data-forceCover="1" 
                         data-aspectratio="16:9" 
                         data-autoplay="true" 
                         data-autoplayonlyfirsttime="false" 
                         ></div>
                    <!-- <div class="rs-background-video-layer z-index-10 " 
                          data-forcerewind="on" 
                          data-volume="mute" 
                          data-videowidth="100%" 
                          data-videoheight="100%" 
                          data-videomp4="video/Network_Executive_Sequence_4.mp4" 
                          data-videopreload="preload" 
                          data-videoloop="loop" 
                          data-forceCover="1" 
                          data-aspectratio="16:9" 
                          data-autoplay="true" 
                          data-autoplayonlyfirsttime="false" 
                          data-nextslideatend="false"></div>
                    -->

                    <!-- 	
                            <div class="overlay overlay-color-dark overlay-show overlay-op-8"></div> https://stackoverflow.com/questions/43606251/using-timeupdate-and-currenttime-->

                </li>
            </ul>
        </div>
    </div>
    <div class="cropos">
        <div class="carousel-item1 cap1" style="display: none;">

            <div class="carousel-caption1">
                <div class="row justify-content-end">
                    <div class="col-md-6">

                        <div class="carousel-caption1 text-right">
                            <h5>GLOBAL <span>RECRUITMENT SOLUTIONS</span></h5>
                            <p>We support employers in recruiting their Mid to Board level talent across all functions spanning EMEA, The Americas, and APAC</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item1 cap2" style="display: none;">
            <div class="carousel-caption1">
                <div class="row justify-content-start">
                    <div class="col-md-6">

                        <div class="carousel-caption1 text-left">

                            <h5><span>TRUSTED RECRUITMENT</span> PARTNER</h5>
                            <p>Our clients utilise our expertise to solve many difficult recruitment challenges in different countries and across multiple functions"</p>
                        </div>
                    </div>
                    <div class="col-md-6"><h5 class="text-right" style="padding-right: 30px">UNITED KINGDOM</h5></div>
                </div>
            </div>
        </div>
        <div class="carousel-item1 cap3" style="display: none;">
            <div class="carousel-caption1">
                <div class="row justify-content-start">
                    <div class="col-md-6">

                        <div class="carousel-caption1 text-left">

                            <h5><span>FLEXIBLE AND ADAPTABLE</span> RECRUITMENT SOLUTIONS</h5>
                            <p>We provide a wide range of recruitment services to support the needs of our clients and offer a flexible and adaptable approach to resolving their recruitment challenges</p>
                        </div>
                    </div>
                    <div class="col-md-6"><h5 class="text-right" style="padding-right: 30px">EUROPE</h5></div>
                </div>
            </div>
        </div>














        <div class="carousel-item1 cap4" style="display: none;">

            <div class="carousel-caption1 ">
                <div class="row justify-content-end">
                    <div class="col-md-6"><h5 class="text-left" style="padding-left: 30px">AMERICAS</h5></div>
                    <div class="col-md-6">

                        <div class="carousel-caption1 text-right" >

                            <h5><span>ACCOUNT</span> MANAGEMENT</h5>
                            <p>With a wealth of experience in managing complex and confidential assignments we are proud of the long-standing relationships we maintain with our clients and the positive impact we have on their performance</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="carousel-item1 cap5" style="display: none;">

            <div class="carousel-caption1">
                <div class="row justify-content-start">
                    <div class="col-md-6">

                        <div class="carousel-caption1 text-left" >

                            <h5>STRATEGIC <span>RECRUITMENT PARTNER</span></h5>
                            <p>Acting as a trusted advisor we source individuals with specific skills to enable our clients to realise their strategic goals</p>
                        </div>
                    </div>
                    <div class="col-md-6"><h5 class="text-right" style="padding-right: 30px">MIDDLE EAST</h5></div>
                </div>
            </div>
        </div>
        <div class="carousel-item1 cap6" style="display: none;">

            <div class="carousel-caption1">
                <div class="row justify-content-end">
                    <div class="col-md-6"><h5 class="text-left" style="padding-left: 30px">AUSTRALIA </h5></div>
                    <div class="col-md-6">

                        <div class="carousel-caption1 text-right">

                            <h5><span>RELATIONSHIP</span> MANAGEMENT</h5>
                            <p>We develop and maintain different relationships with our clients based on how they like to work and are structured into four divisions to deliver solutions that best match their needs</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="carousel-item1 cap7" style="display: none;">

            <div class="carousel-caption1 ">
                <div class="row justify-content-start">
                    <div class="col-md-6">

                        <div class="carousel-caption1 text-left ">

                            <h5><span>ACCOUNT</span> MANAGEMENT</h5>
                            <p>With a wealth of experience in managing complex and confidential assignments we are proud of the long-standing relationships we maintain with our clients and the positive impact we have on their performance</p>
                        </div>
                    </div>
                    <div class="col-md-6"><h5 class="text-right"style="padding-right: 30px">ASIA</h5></div>
                </div>
            </div>
        </div>
        <div class="carousel-item1 cap8" style="display: none;">

            <div class="carousel-caption1 ">
                <div class="row justify-content-start">
                    <div class="col-md-6"><h5 class="text-left" style="padding-left: 30px">AFRICA</h5></div>
                    <div class="col-md-6">

                        <div class="carousel-caption1 text-right">

                            <h5>DELIVERING YOUR <span>RESOURCING NEEDS</span></h5>
                            <p>We deliver recruitment services that provide a fast and effective solution to meet your business resourcing needs.  </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>



    </div>

                <!-- 	<section id="start" class="section p-0" style="position: absolute;top:10px;width: 100%">
                                        <div class="hreo_bred" style="background: transparent;">

                                                <div class="container-fluid">
                                                <div class="row justify-content-center">
                                                <div class="col-md-10">
                                                                
                                                                        <div class="herroinner text-right">
                                                                        <h3><img src="images/network-executive.png"></h3>
                                                                        <h4>Global search solutions  that identify and <br> attract the talent your business needs</h4>

                                                                </div>
                                                        </div>

                                                        </div>
                                                </div>
                                        </div>

                                </section> -->
                <!-- 			<section id="start" class="section diviSection" style="padding: 50px 0 20px"> -->
    <section id="start" class="section diviSection" style="padding: 50px 0 20px">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">
                                    <span class="top-sub-title">About</span>
                                    <h2 class="font-weight-bold text-4 mb-3">Your recruitment specialist</h2>
                                    <p>We are a global recruitment solutions business supporting employers with recruiting across all their functions. Based in the UK and Netherlands we source and attract exceptional talent and have established an outstanding reputation for delivering results.</p>
                                    <p>
We provide a wide range of recruitment services to support the needs of our clients and offer a flexible and adaptable approach to resolving their recruitment challenges. With a wealth of experience in managing complex, challenging and confidential assignments we are proud of the long-standing relationships we maintain with our clients and the positive impact we have on their performance.</p><p>
We develop and maintain different relationships with our clients based on how they like to work and are structured into four divisions to deliver solutions that best match their needs.
</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 about_Section ">

                            <div class="about_space"  onclick="window.location.href = 'division-executive'" style="cursor: pointer;">
                                <div class="aboutspece_inner px-2">
                                    <h2 class="text-center"><a href="division-executive"><img alt="Network Executive" width="" src="<?php echo base_url() ?>assets/themes/front/images/net-executive-white.png"></a></h2>
                                    <p style="font-size: 14px;">Global search <br>solutions that identify <br>and attract the talent<br> your business needs </p>
                                </div>


                            </div>

                        </div>
                        <div class="col-md-3 about_Section " >
                            <div class="about_space" onclick="window.location.href = 'division-professional'"  style="cursor: pointer;">
                                <div class="aboutspece_inner px-2">
                                    <h2 class="text-center"><a href="division-professional"><img alt="Network Professional" width="" src="<?php echo base_url() ?>assets/themes/front/images/net-professional-white.png"></a></h2>
                                    <p style="font-size: 14px;">Recruitment services that <br>deliver solutions to meet
your <br>middle to senior professional<br> and executive needs
</p>
                                </div>


                            </div>

                        </div>
                        <div class="col-md-3 about_Section " >
                            <div class="about_space" onclick="window.location.href = 'division-recruitment'"  style="cursor: pointer;">
                                <div class="aboutspece_inner px-2">
                                    <h2 class="text-center"><a href="division-recruitment"><img alt="Network Recruitment" width="" src="<?php echo base_url() ?>assets/themes/front/images/net-recruitment-white.png"></a></h2>
                                    <p style="font-size: 14px;">Recruitment services<br> that deliver solutions to meet<br>
your junior, transactional <br>and part-qualified needs
</p>
                                </div>


                            </div>

                        </div>
                         <div class="col-md-3 about_Section " >
                            <div class="about_space" onclick="window.location.href = 'division-direct'"  style="cursor: pointer;">
                                <div class="aboutspece_inner px-2">
                                    <h2 class="text-center"><a href="division-direct"><img alt="Network Direct" width="" src="<?php echo base_url() ?>assets/themes/front/images/netdirect-white.png"></a></h2>
                                    <p style="font-size: 14px;">Recruitment services<br> that deliver cost-effective <br>solutions that connect you <br>
directly with talent 

</p>
                                </div>


                            </div>

                        </div>



                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class=" curacancies">
        <div class="container">
            <div class="text-center"><h2>Current Vacancies</h2></div>
            <div class="row">
                <div class="owl-carousel" id="job-slide">

                    <!-- Single Job -->
                    <?php if($carousel_data) {
                        foreach($carousel_data as $v){
                        ?>
                    <div class="item">
                        <div class="job-grid style-1">
                            <div class="job-grid-wrap">

                                <h4 class="job-title"><a href=""><?php echo $v->designation ?></a></h4>
                                <hr>
                                <div class="job-grid-detail">
                                    <h4 class="jbc-name"><a href=""><i class="fa fa-map-marker-alt"></i> <?php echo $v->location ?></a></h4>

                                </div>
                                <div class="job-grid-footer">
                                    <h4 class="job-price"><?php echo $v->currency_symbol.$v->amount; ?></h4>

                                </div>

                            </div>
                        </div>
                    </div>
                    <?php } } ?>
                


                </div>
            </div>
        </div>
    </section>

    <div class="newstart">
        <div class="text-center"><h2>Local Solutions - Global Reach</h2></div>

        <div class="about0map">
            <img class="img-fluid" src="<?php echo base_url() ?>assets/themes/front/images/map-index.png">
        </div>



    </div>	


    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg2">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">
                        <div class="row ">
                            <div class="col-md-12">

                                <div class="herroinner text-right">
                                    <h3>What’s next?</h3>
                                    <h4>Contact Us Today</h4>
                                    <p class="details"><strong>United Kingdom</strong> - +44 (0) 1962 677013 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details"><strong>Netherlands</strong> - +31 (0) 20 854 6378 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details">info@netrecruit.com <i class="fas fa-envelope"></i></p>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


</div>