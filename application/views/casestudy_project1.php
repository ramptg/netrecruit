<div role="main" class="main">
    <section id="start" class="section bg-light-5 p-0">
        <div class="hreo_bred">

            <div class="container-fluid h-100">
                <div class="row justify-content-end align-items-center h-100">
                    <div class="col-md-4 ">
                        <h3 class="casepro1"><?php echo $result->title; ?></h3>
                    </div>
                    <div class="col-md-6 order-first order-md-last">

                        <div class="herroinner text-right">
                            <h3><img src="<?php echo base_url() ?>assets/themes/front/images/network-executive.png"></h3>
                            <h4><?php echo $result->short_description; ?></h4>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <section id="start" class="section diviSection">
        <div class="container-fluid">
            <?php echo html_entity_decode($result->content) ?>
<!--            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">
                                         <span class="top-sub-title">Divisions</span> 
                                    <h2 class="font-weight-bold text-4 mb-3">The Task</h2>
                                    <p> A pioneering British Biotech firm, experiencing strong growth, needed a Facilities Management Interim Director to plan and implement a major office move whilst maintaining the business’ cutting-edge laboratory facilities and accommodating all the necessary business support functions. The right candidate needed to have experience of both supporting sensitive research/laboratorial facilities and successfully relocating business operations.   </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
<!--            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">
                                         <span class="top-sub-title">Divisions</span> 
                                    <h2 class="font-weight-bold text-4 mb-3">Our Approach & Result</h2>
                                    <p> Network Executive produced a shortlist of highly relevant, interviewed and assessed, candidates at a range of price points within two days. The appointed candidate delivered a highly successful move resulting in reengagement on some other critical projects. At the end of the interim contract, the candidate was offered a permanent move to stay with the Biotech firm.   </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
    </section>


    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg2">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">
                        <div class="row ">
                            <div class="col-md-12">

                                <div class="herroinner text-right">
                                    <h3>What’s next?</h3>
                                    <h4>Contact Us Today</h4>
                                    <p class="details"><strong>United Kingdom</strong> - +44 (0) 1962 677013 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details"><strong>Netherlands</strong> - +31 (0) 20 854 6378 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details">info@netrecruit.com <i class="fas fa-envelope"></i></p>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


</div>