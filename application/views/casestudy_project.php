<div role="main" class="main">
    <section id="start" class="section service_conslt case_bg  bg-light-5 p-0">
        <div class="hreo_bred">

            <div class="container-fluid h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">

                        <div class="herroinner text-right">
                            <h3>Case Studies</h3>
                            <!-- <h4>Our exceptionally talented <br>& motivated team.</h4> -->

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <section id="start" class="section case_section">
        <div class="container">
            <div class="row justify-content-center">
                <?php if($case_studies_sub_cat){ 
                    foreach($case_studies_sub_cat as $val){
                    ?>
                <div class="col-md-4 about_Section ">
                    <div class="about_space"  onclick="window.location.href = '<?php echo base_url('casestudy-project1/'.$val->id); ?>'" style="cursor: pointer;">
                        <div class="aboutspece_inner">
                            <h2 class="text-center"><a href="<?php echo base_url('casestudy-project1/'.$val->id); ?>"><?php echo $val->title; ?></a></h2>
                            <p><?php echo $val->short_description; ?></p>
                        </div>
                    </div>
                </div>
                    <?php } } ?>
            </div>
        </div>
    </section>


    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg2">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">
                        <div class="row ">
                            <div class="col-md-12">

                                <div class="herroinner text-right">
                                    <h3>What’s next?</h3>
                                    <h4>Contact Us Today</h4>
                                    <p class="details"><strong>United Kingdom</strong> - +44 (0) 1962 677013 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details"><strong>Netherlands</strong> - +31 (0) 20 854 6378 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details">info@netrecruit.com <i class="fas fa-envelope"></i></p>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


</div>