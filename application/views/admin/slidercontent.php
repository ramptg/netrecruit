<style>
    .required{color:red;}
</style>
<div class="content-wrapper">
    <!-- Page Title Header Starts-->

    <!-- Page Title Header Ends-->
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Manage Slider Content</h4>

                    <?php if (isset($message)) {
                        ?><div class="alert alert-success text-left"><?php
                        echo $message;
                        ?></div><?php }
                    ?>

                    <?php if (isset($error)) {
                        ?><div class="alert alert-danger text-left"><?php
                        echo $error;
                        ?></div><?php }
                    ?>
                    <form method="post" id="form_data_submit" class="login-form" action="<?php echo base_url('dashboard/slidercontent') ?>">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">First Content <span class="required">*</span></label>
                                    <div class="">
                                        <textarea required="" name="first_content" class="form-control"><?php echo isset($result->first_content) && !empty($result->first_content) ? $result->first_content : '' ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label">Second  Content <span class="required">*</span></label>
                                    <div class="">
                                        <textarea required="" name="second_content" class="form-control"><?php echo isset($result->second_content) && !empty($result->second_content) ? $result->second_content : '' ?></textarea>


                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">Youtube URL <span class="required">*</span></label>
                                    <div class="">
                                        <input type="url" required="" name="youtube_url" value="<?php echo isset($result->youtube_url) && !empty($result->youtube_url) ? $result->youtube_url : '' ?>" class="form-control"/>


                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class=" col-form-label">Status <span class="required"></span></label>
                                    <div class="">
                                        <select class="form-control" name="status">
                                            <option <?php echo isset($result->status) && $result->status == '0' ? 'selected' : '' ?> value="0">Active</option>
                                            <option <?php echo isset($result->status) && $result->status == '1' ? 'selected' : '' ?> value="1">Deactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                             

                            <input type="hidden" name="id" value="<?php echo isset($result->id) && !empty($result->id) ? $result->id : '' ?>"/>

                        </div>
                        <button type="submit" class="btn btn-success mr-2">Submit <img src="<?php echo base_url() ?>assets/themes/admin/images/lazyLoadImage.gif" id="loaderjob" style="font-size:24px;width: 31px; left: 90px; top: 44px;" class="loaders display-hide"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
