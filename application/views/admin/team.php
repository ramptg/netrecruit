<style>
    .required{color:red;}
</style>
<div class="content-wrapper">
    <!-- Page Title Header Starts-->

    <!-- Page Title Header Ends-->
    <div class="row">
        <div class="col-md-12 grid-margin">
            <form method="post" id="form_data_submit" class="login-form" action="<?php echo base_url('dashboard/team/') ?>">
                <div class="card">
                    <div class="card-body">
                        <?php if (isset($message)) {
                            ?><div class="alert alert-success text-left"><?php
                            echo $message;
                            ?></div><?php }
                        ?>
                        <?php if (isset($error)) {
                            ?><div class="alert alert-danger text-left"><?php
                                echo $error;
                                ?></div><?php }
                            ?>
                   
                        <h4 class="card-title">Manage Team Content</h4>
                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Slider Title <span class="required">*</span></label>
                                    <div class="">
                                        <input value="<?php echo isset($result->slider_title) && !empty($result->slider_title) ? $result->slider_title : '' ?>" type="text" required="" name="slider_title" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Slider Content <span class="required">*</span></label>
                                    <div class="">
                                        <textarea id="summernote" type="text" required="" name="slider_content" class="form-control"><?php echo isset($result->slider_content) && !empty($result->slider_content) ? $result->slider_content : '' ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Our Team</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Title  <span class="required">*</span></label>
                                    <div class="">
                                        <input value="<?php echo isset($result->ftitle) && !empty($result->ftitle) ? $result->ftitle : '' ?>" type="text" required="" name="ftitle" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label">Manage Content<span class="required">*</span></label>
                                    <div class="">
                                        <textarea required="" id="summernote_3" name="fcontent" class="form-control"><?php echo isset($result->fcontent) && !empty($result->fcontent) ? $result->fcontent : '' ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <input type="hidden" name="id" value="1"/>
                         <button type="submit" class="btn btn-success mr-2">Submit <img src="<?php echo base_url() ?>assets/themes/admin/images/lazyLoadImage.gif" id="loaderjob" style="font-size:24px;width: 31px; left: 90px; top: 44px;" class="loaders display-hide"></button>
                    </div>
                </div>
            </form>

        </div>
    </div>


</div>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
