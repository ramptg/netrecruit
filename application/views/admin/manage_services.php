<style>
    .required{color:red;}
</style>
<div class="content-wrapper">
    <!-- Page Title Header Starts-->

    <!-- Page Title Header Ends-->
    <div class="row">
        <div class="col-md-12 grid-margin">

            <form method="post" id="form_data_submit" class="login-form" action="<?php echo base_url('dashboard/manage_services/') ?>">


                <div class="card">
                    <div class="card-body">
                        <?php if (isset($message)) {
                            ?><div class="alert alert-success text-left"><?php
                            echo $message;
                            ?></div><?php }
                        ?>
                        <?php if (isset($error)) {
                            ?><div class="alert alert-danger text-left"><?php
                                echo $error;
                                ?></div><?php }
                            ?>
                        <?php
                        if ($slug == 1) {
                            $title = 'Manage Direct Hiring Solutions';
                        } elseif ($slug == 2) {
                            $title = 'Manage Search';
                        } elseif ($slug == 3) {
                            $title = 'Manage Advertised Selection';
                        } elseif ($slug == 3) {
                            $title = 'Manage Database Search';
                        }
                        ?>
                        <h4 class="card-title"><?= $title ?> </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Slider Title <span class="required">*</span></label>
                                    <div class="">
                                        <input value="<?php echo isset($result->slider_title) && !empty($result->slider_title) ? $result->slider_title : '' ?>" type="text" required="" name="slider_title" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Slider Content <span class="required">*</span></label>
                                    <div class="">
                                        <textarea  id="summernote" required="" name="slider_content" class="form-control"><?php echo isset($result->slider_content) && !empty($result->slider_content) ? $result->slider_content : '' ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">First Section</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Title  <span class="required">*</span></label>
                                    <div class="">
                                        <input value="<?php echo isset($result->ftitle) && !empty($result->ftitle) ? $result->ftitle : '' ?>" type="text" required="" name="ftitle" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label">Manage Content<span class="required">*</span></label>
                                    <div class="">
                                        <textarea required="" name="fcontent" class="form-control"><?php echo isset($result->fcontent) && !empty($result->fcontent) ? $result->fcontent : '' ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Second Section</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Title  <span class="required">*</span></label>
                                    <div class="">
                                        <input value="<?php echo isset($result->stitle) && !empty($result->stitle) ? $result->stitle : '' ?>" type="text" required="" name="stitle" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label">Manage Content<span class="required">*</span></label>
                                    <div class="">
                                        <textarea required="" name="scontent" class="form-control"><?php echo isset($result->scontent) && !empty($result->scontent) ? $result->scontent : '' ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Third Section</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Title  <span class="required">*</span></label>
                                    <div class="">
                                        <input value="<?php echo isset($result->thtitle) && !empty($result->thtitle) ? $result->thtitle : '' ?>" type="text" required="" name="thtitle" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label">Manage Content<span class="required">*</span></label>
                                    <div class="">
                                        <textarea required="" name="thcontent" class="form-control"><?php echo isset($result->thcontent) && !empty($result->thcontent) ? $result->thcontent : '' ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <?php if ($slug == 1) { ?>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Four Section</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class=" col-form-label">Title  <span class="required">*</span></label>
                                        <div class="">
                                            <input value="<?php echo isset($result->fourtitle) && !empty($result->fourtitle) ? $result->fourtitle : '' ?>" type="text" required="" name="fourtitle" class="form-control"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Manage Content<span class="required">*</span></label>
                                        <div class="">
                                            <textarea required="" name="fourcontent" class="form-control"><?php echo isset($result->fourcontent) && !empty($result->fourcontent) ? $result->fourcontent : '' ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php } ?>
                <input type="hidden" name="slug" value="<?= $result->slug ?>"/>
                <button type="submit" class="btn btn-success mr-2">Submit <img src="<?php echo base_url() ?>assets/themes/admin/images/lazyLoadImage.gif" id="loaderjob" style="font-size:24px;width: 31px; left: 90px; top: 44px;" class="loaders display-hide"></button>

            </form>

        </div>
    </div>


</div>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
