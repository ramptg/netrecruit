<style>
    .required{color:red;}
</style>
<div class="content-wrapper">
    <!-- Page Title Header Starts-->

    <!-- Page Title Header Ends-->
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Manage Carousel</h4>

                    <?php if (isset($message)) {
                        ?><div class="alert alert-success text-left"><?php
                        echo $message;
                        ?></div><?php }
                    ?>

                    <?php if (isset($error)) {
                        ?><div class="alert alert-danger text-left"><?php
                        echo $error;
                        ?></div><?php }
                    ?>
                    <form method="post" id="form_data_submit" enctype="multipart/form-data" class="login-form" action="<?php echo base_url('dashboard/carousel_detail') ?>">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class=" col-form-label">Designation <span class="required">*</span></label>
                                    <div class="">
                                        <input autocomplete="off" value="<?php echo isset($res->designation) && !empty($res->designation) ? $res->designation : '' ?>" type="text" required="" name="designation" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">Location <span class="required">*</span></label>
                                    <div class="">
                                        <input autocomplete="off" type="text" id="" value="<?php echo isset($res->location) && !empty($res->location) ? $res->location : '' ?>" required="" name="location" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">Currency symbol <span class="required">*</span></label>
                                    <div class="">
                                        <input autocomplete="off" type="text" id="" value="<?php echo isset($res->currency_symbol) && !empty($res->currency_symbol) ? $res->currency_symbol : '' ?>" required="" name="currency_symbol" class="form-control">
                                    </div>
                                </div>
                            </div>
                            
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">Amount <span class="required">*</span></label>
                                    <div class="">
                                        <input autocomplete="off" type="text" id="" value="<?php echo isset($res->amount) && !empty($res->amount) ? $res->amount : '' ?>" required="" name="amount" class="form-control CurrencyInput">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class=" col-form-label">Status <span class="required"></span></label>
                                    <div class="">
                                        <select class="form-control" name="status">
                                            <option <?php echo isset($res->status) && $res->status == '0' ? 'selected' : '' ?> value="0">Active</option>
                                            <option <?php echo isset($res->status) && $res->status == '1' ? 'selected' : '' ?> value="1">Deactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>



                            <input type="hidden" name="id" value="<?php echo isset($res->id) && !empty($res->id) ? $res->id : '' ?>"/>

                        </div>
                        <button type="submit" class="btn btn-success mr-2">Submit <img src="<?php echo base_url() ?>assets/themes/admin/images/lazyLoadImage.gif" id="loaderjob" style="font-size:24px;width: 31px; left: 90px; top: 44px;" class="loaders display-hide"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Carousel Listing <div class="clear"></div></h4>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr>
                                    <th> # </th>
                                    <th>Designation</th>
                                    <th>Location </th>
                                    <th>Amount</th>
                                    <th>Status </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($result) {
                                    foreach ($result as $key => $val) {
                                        ?>
                                        <tr>
                                            <td><?php echo ++$key ?></td>
                                            <td><?php echo $val->designation ?></td>
                                            <td><?php echo $val->location ?></td>
                                            <td><?php echo $val->currency_symbol .$val->amount ?></td>
                                            <?php if ($val->status == '1') { ?>
                                                <td><button type="button" class="btn btn-danger btn-rounded ">Deactive</button></td> 

                                            <?php } else { ?>
                                                <td><button type="button" class="btn btn-success btn-rounded ">Active</button></td>  
                                            <?php } ?>
                                            <td>
                                                  
                                                 <a href="<?php echo base_url('dashboard/carousel_detail/' . $val->id) ?>"  class="btn btn-sm btn-primary">Edit</a>
                                           
                                                <a href="javascript::void(0);" url="<?php echo base_url('dashboard/delete_carousel_detail/' . $val->id) ?>"  class="delete_contact btn btn-sm btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    $('.delete_contact').on('click', function () {
        var url = $(this).attr('url');
        bootbox.confirm({
            message: "Are you sure to delete this section details?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    window.location.href = url;
                }
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        jQuery(".CurrencyInput").on('keyup', function (evt) {
            var value = $(this).val();
            if (evt.which != 110) {//not a fullstop
                var n = parseFloat(jQuery(this).val().replace(/\,/g, ''), 10);
              
              if(isNaN(n)) {
                    $(this).val('');
                }else{
                if (value == '') {
                    $(this).val('');
                } else {
                    jQuery(this).val(n.toLocaleString());
                }
            }
            }
        });

    });
</script>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
