<style>
    .required{color:red;}
</style>
<div class="content-wrapper">
    <!-- Page Title Header Starts-->

    <!-- Page Title Header Ends-->
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Change Password</h4>

                    <?php if (isset($message)) {
                        ?><div class="alert alert-success text-left"><?php
                        echo $message;
                        ?></div><?php }
                    ?>

                    <?php if (isset($error)) {
                        ?><div class="alert alert-danger text-left"><?php
                        echo $error;
                        ?></div><?php }
                    ?>
                    <form method="post" id="form_data_submit" enctype="multipart/form-data" class="login-form" action="<?php echo base_url('auth/change_password') ?>">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Old Password <span class="required">*</span></label>
                                    <div class="">
                                        <input type="password" required="" name="old" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">New Password <span class="required">*</span></label>
                                    <div class="">
                                        <input type="password" required="" name="new" id="password" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Confirm Password <span class="required">*</span></label>
                                    <div class="">
                                        <input type="password" required="" name="confirmpassword" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-success mr-2">Submit <img src="<?php echo base_url() ?>assets/themes/admin/images/lazyLoadImage.gif" id="loaderjob" style="font-size:24px;width: 31px; left: 90px; top: 44px;" class="loaders display-hide"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>



</div>

<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
