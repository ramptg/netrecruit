<style>
    .required{color:red;}
</style>
<div class="content-wrapper">
    <!-- Page Title Header Starts-->

    <!-- Page Title Header Ends-->
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Manage Case Studies</h4>

                    <?php if (isset($message)) {
                        ?><div class="alert alert-success text-left"><?php
                        echo $message;
                        ?></div><?php }
                    ?>

                    <?php if (isset($error)) {
                        ?><div class="alert alert-danger text-left"><?php
                        echo $error;
                        ?></div><?php }
                    ?>
                    <form method="post" id="form_data_submit" enctype="multipart/form-data" class="login-form" action="<?php echo base_url('dashboard/case_studies_sub_cat') ?>">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Title Name <span class="required">*</span></label>
                                    <div class="">
                                        <input autocomplete="off" value="<?php echo isset($res->title) && !empty($res->title) ? $res->title : '' ?>" type="text" required="" name="title" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label">Manage Short Description<span class="required">*</span></label>
                                    <div class="">
                                        <textarea id="" required="" name="short_description" class="form-control"><?php echo isset($res->short_description) && !empty($res->short_description) ? $res->short_description : '' ?></textarea>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label">Manage Page Content<span class="required">*</span></label>
                                    <div class="">
                                        <textarea id="summernote_3"  required="" name="content" class="form-control"><?php echo isset($res->content) && !empty($res->content) ? $res->content : '' ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class=" col-form-label">Status <span class="required"></span></label>
                                    <div class="">
                                        <select class="form-control" name="status">
                                            <option <?php echo isset($res->status) && $res->status == '0' ? 'selected' : '' ?> value="0">Active</option>
                                            <option <?php echo isset($res->status) && $res->status == '1' ? 'selected' : '' ?> value="1">Deactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                             <input type="hidden" name="caseid" value="<?php echo $caseid; ?>"/>

                            <input type="hidden" name="id" value="<?php echo isset($res->id) && !empty($res->id) ? $res->id : '' ?>"/>

                        </div>
                        <button type="submit" class="btn btn-success mr-2">Submit <img src="<?php echo base_url() ?>assets/themes/admin/images/lazyLoadImage.gif" id="loaderjob" style="font-size:24px;width: 31px; left: 90px; top: 44px;" class="loaders display-hide"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Case Studies Listing <div class="clear"></div></h4>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr>
                                    <th> # </th>
                                    <th>Title Name</th>
                                    <th>Short Description</th>
<!--                                    <th>Page Content </th>-->
                                    <th>Status </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($result) {
                                    foreach ($result as $key => $val) {
                                        ?>
                                        <tr>
                                            <td><?php echo ++$key ?></td>
                                            <td><?php echo $val->title ?></td>
                                            <td style="word-break: break-all; white-space: normal;"><?php echo html_entity_decode($val->short_description) ?></td>
<!--                                            <td style="word-break: break-all; white-space: normal;"><?php //echo html_entity_decode($val->content) ?></td>-->
                                            <?php if ($val->status == '1') { ?>
                                                <td><button type="button" class="btn btn-danger btn-rounded ">Deactive</button></td> 

                                            <?php } else { ?>
                                                <td><button type="button" class="btn btn-success btn-rounded ">Active</button></td>  
                                            <?php } ?>
                                            <td>
                                                   
                                                 <a href="<?php echo base_url('dashboard/case_studies_sub_cat/'.$caseid."/" . $val->id) ?>"  class="btn btn-sm btn-primary">Edit</a>
                                           
                                                <a href="javascript::void(0);" url="<?php echo base_url('dashboard/delete_case_studies_sub_cat/'.$caseid."/" . $val->id) ?>"  class="delete_contact btn btn-sm btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    $('.delete_contact').on('click', function () {
        var url = $(this).attr('url');
        bootbox.confirm({
            message: "Are you sure to delete this section details?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    window.location.href = url;
                }
            }
        });
    });
</script>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
