<style>
    .required{color:red;}
</style>
<div class="content-wrapper">
    <!-- Page Title Header Starts-->

    <!-- Page Title Header Ends-->
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Multi-functional Expertise (Network Executive)</h4>

                    <?php if (isset($message)) {
                        ?><div class="alert alert-success text-left"><?php
                        echo $message;
                        ?></div><?php }
                    ?>

                    <?php if (isset($error)) {
                        ?><div class="alert alert-danger text-left"><?php
                        echo $error;
                        ?></div><?php }
                    ?>
                    <form method="post" id="form_data_submit" enctype="multipart/form-data" class="login-form" action="<?php echo base_url('dashboard/exe_multi_fun_expertise') ?>">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Title <span class="required">*</span></label>
                                    <div class="">
                                        <input value="<?php echo isset($result->title) && !empty($result->title) ? $result->title : '' ?>" type="text" required="" name="title" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class=" col-form-label">Status <span class="required"></span></label>
                                    <div class="">
                                        <select class="form-control" name="status">
                                            <option <?php echo isset($result->status) && $result->status == '0' ? 'selected' : '' ?> value="0">Active</option>
                                            <option <?php echo isset($result->status) && $result->status == '1' ? 'selected' : '' ?> value="1">Deactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">Upload Image<span class="required">*</span></label>
                                    <div class="">
                                        <input type="file" name="file" value="" class="form-control"/>
                                    </div>
                                </div>
                                <p class="small"><i class="fa fa-info-circle"></i><b>Note-:</b> Ideal image size is (Width-198px , Height-198px). Images of any other size may appear stretched. </p>
                            
                                <?php if($result->file){ ?>
                                <a style="float:right;" target="_blank" href="<?php echo base_url('upload/'.$result->file) ?>"  class="btn btn-light">View File</a>
                                <?php } ?>
                            </div>

                            <input type="hidden" name="id" value="<?php echo isset($result->id) && !empty($result->id) ? $result->id : '' ?>"/>
                            <input type="hidden" name="slug" value="1"/>
                        </div>
                        <button type="submit" class="btn btn-success mr-2">Submit <img src="<?php echo base_url() ?>assets/themes/admin/images/lazyLoadImage.gif" id="loaderjob" style="font-size:24px;width: 31px; left: 90px; top: 44px;" class="loaders display-hide"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Multi-Functional Expertise (Network Executive) <div class="clear"></div></h4>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr>
                                    <th> # </th>
                                    <th>Title</th>
                                    <th>Status </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($section_data) {
                                    foreach ($section_data as $key => $val) {
                                        ?>
                                        <tr>
                                            <td><?php echo ++$key ?></td>
                                            <td><?php echo $val->title ?></td>
                                      
                                            <?php if ($val->status == '1') { ?>
                                                <td><button type="button" class="btn btn-danger btn-rounded ">Deactive</button></td> 

                                            <?php } else { ?>
                                                <td><button type="button" class="btn btn-success btn-rounded ">Active</button></td>  
                                            <?php } ?>
                                            <td>
                                                <a href="<?php echo base_url('dashboard/exe_multi_fun_expertise/' . $val->id) ?>"  class="btn btn-sm btn-primary">Edit</a>
                                                <a href="javascript::void(0);" url="<?php echo base_url('dashboard/delete_exe_multi_fun_expertise/' . $val->id) ?>"  class="delete_contact btn btn-sm btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.delete_contact').on('click', function () {
        var url = $(this).attr('url');
        bootbox.confirm({
            message: "Are you sure to delete this detail?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    window.location.href = url;
                }
            }
        });
    });
</script>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
