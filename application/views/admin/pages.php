<style>
    .required{color:red;}
</style>
<div class="content-wrapper">
    <!-- Page Title Header Starts-->

    <!-- Page Title Header Ends-->
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?php 
                    if($page_id == 1){
                        echo "Terms & Conditions";
                    }elseif($page_id == 2){
                        echo "Privacy Policy";
                    }elseif($page_id == 3){
                        echo "Cookie Policy";
                    }elseif($page_id == 4){
                        echo "Data Retention";
                    }
                    ?></h4>

                    <?php if (isset($message)) {
                        ?><div class="alert alert-success text-left"><?php
                        echo $message;
                        ?></div><?php }
                    ?>

                    <?php if (isset($error)) {
                        ?><div class="alert alert-danger text-left"><?php
                        echo $error;
                        ?></div><?php }
                    ?>
                    <form method="post" id="form_data_submit" class="login-form" action="<?php echo base_url('dashboard/pages') ?>">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class=" col-form-label">Page Title <span class="required">*</span></label>
                                    <div class="">
                                        <input value="<?php echo isset($result->page_title) && !empty($result->page_title) ? $result->page_title : '' ?>" type="text" required="" name="page_title" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label">Description <span class="required">*</span></label>
                                    <div class="">
                                        <textarea id="summernote_2"  required="" name="text" class="form-control"><?php echo isset($result->text) && !empty($result->text) ? $result->text : '' ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="<?php echo isset($page_id) && !empty($page_id) ? $page_id : '' ?>"/>

                        </div>
                        <button type="submit" class="btn btn-success mr-2">Submit <img src="<?php echo base_url() ?>assets/themes/admin/images/lazyLoadImage.gif" id="loaderjob" style="font-size:24px;width: 31px; left: 90px; top: 44px;" class="loaders display-hide"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
