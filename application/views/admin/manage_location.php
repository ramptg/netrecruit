<style>
    .required{color:red;}
      input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        margin: 0; 
    }

    input[type=number]{
        -moz-appearance:textfield;}
</style>
<div class="content-wrapper">
    <!-- Page Title Header Starts-->

    <!-- Page Title Header Ends-->
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Manage Location</h4>

                    <?php if (isset($message)) {
                        ?><div class="alert alert-success text-left"><?php
                        echo $message;
                        ?></div><?php }
                    ?>

                    <?php if (isset($error)) {
                        ?><div class="alert alert-danger text-left"><?php
                        echo $error;
                        ?></div><?php }
                    ?>
                    <form method="post" id="form_data_submit" class="login-form" action="<?php echo base_url('dashboard/manage_location') ?>">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class=" col-form-label">City Name <span class="required">*</span></label>
                                    <div class="">
                                        <input value="<?php echo isset($result->city_name) && !empty($result->city_name) ? $result->city_name : '' ?>" type="text" required="" name="city_name" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">Latitude<span class="required">*</span></label>
                                    <div class="">
                                        <input maxlength="12" value="<?php echo isset($result->latitude) && !empty($result->latitude) ? $result->latitude : '' ?>" type="number" required="" name="latitude" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-form-label">Longitude<span class="required">*</span></label>
                                    <div class="">
                                        <input maxlength="12" value="<?php echo isset($result->longitude) && !empty($result->longitude) ? $result->longitude : '' ?>" type="number" required="" name="longitude" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label class=" col-form-label">Status <span class="required"></span></label>
                                    <div class="">
                                        <select class="form-control" name="status">
                                            <option <?php echo isset($result->status) && $result->status == '0' ? 'selected' : '' ?> value="0">Active</option>
                                            <option <?php echo isset($result->status) && $result->status == '1' ? 'selected' : '' ?> value="1">Deactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <input type="hidden" name="id" value="<?php echo isset($result->id) && !empty($result->id) ? $result->id : '' ?>"/>

                        </div>
                        <button type="submit" class="btn btn-success mr-2">Submit <img src="<?php echo base_url() ?>assets/themes/admin/images/lazyLoadImage.gif" id="loaderjob" style="font-size:24px;width: 31px; left: 90px; top: 44px;" class="loaders display-hide"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Location Listing <div class="clear"></div></h4>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                            <thead>
                                <tr>
                                    <th> # </th>
                                    <th>City Name</th>
                                    <th>Latitude </th>
                                    <th>Longitude </th>
                                    <th>Status </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($section_data) {
                                    foreach ($section_data as $key => $val) {
                                        ?>
                                        <tr>
                                            <td><?php echo ++$key ?></td>
                                            <td><?php echo $val->city_name ?></td>
                                            <td><?php echo $val->latitude ?></td>
                                             <td><?php echo $val->longitude ?></td>
                                            <?php if ($val->status == '1') { ?>
                                                <td><button type="button" class="btn btn-danger btn-rounded ">Deactive</button></td> 

                                            <?php } else { ?>
                                                <td><button type="button" class="btn btn-success btn-rounded ">Active</button></td>  
                                            <?php } ?>
                                            <td>
                                                <a href="<?php echo base_url('dashboard/manage_location/' . $val->id) ?>"  class="btn btn-sm btn-primary">Edit</a>
                                                <a href="javascript::void(0);" url="<?php echo base_url('dashboard/delete_location/' . $val->id) ?>"  class="delete_contact btn btn-sm btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<script>
    $('.delete_contact').on('click', function () {
        var url = $(this).attr('url');
        bootbox.confirm({
            message: "Are you sure to delete this location details?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    window.location.href = url;
                }
            }
        });
    });
</script>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
