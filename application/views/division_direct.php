       
       <style>
           .employerstate{font-size: 22px;
    font-weight: 500;
    padding: 9px 10px 10px 0;
  
    display: inline-block;
    margin-top: 10px;
    border-radius: 10px;
    background: #172235;}
          .employerstate span{background: #2c407a;
    padding: 4px 11px 5px;
    border-radius: 10px;
    margin-right: 12px;}
    .employerstate1{padding-right:105px;}
       </style>
       <div role="main" class="main">
                <section id="start" class="section service_conslt service_direct  bg-light-5 p-0">
                    <div class="hreo_bred netdirect">

                        <div class="container-fluid h-100">
                            <div class="row justify-content-center align-items-center h-100">
                                <div class="col-md-10">

                                    <div class="herroinner text-right">
                                        <h3><img src="<?php echo base_url() ?>assets/themes/front/images/netdirect-white.png"></h3>
                                        <h4>Recruitment services that deliver direct hiring solutions <br>connecting  you directly with talent </h4>
  <div class="employerstate">
                                               
                                           <span>Employers:</span> <a href="https://www.directhiringsolutions.com/referral_url/9C3227D64E" style="color: #fff;padding-right: 15px">Register</a>  <a href="https://www.directhiringsolutions.com/referral_url/9C3227D64E" style="color: #fff">Login </a> </div>    <br>
                                           <div class="employerstate employerstate1">
                                               
                                           <span>Candidates:</span>   <a target="_blank" href="<?php echo CANDIDATE_LOGIN ?>" style="color: #fff">Login </a> </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </section>
                <section id="start" class="section diviSection">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <div class="row align-items-baseline mt-2">
                                    <div class="col-lg-12">
                                        <div class="icon-box " data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                            <div class="icon-box-info">
                                                <span class="top-sub-title">Divisions</span>
                                                <h2 class="font-weight-bold text-4 mb-3">Net Direct</h2>
                                                <p>Net  Direct have partnered with <a target="_blank" href="https://www.directhiringsolutions.com/">Direct Hiring Solutions </a>to offer a cost-effective service to support all your recruitment requirements. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </section>
                <section id="start" class="section  bg-light-5 p-0">
                    <div class="hreo_bred permBg directBg">

                        <div class="container-fluid  h-100">
                            <div class="row justify-content-center align-items-center h-100">
                                <div class="col-md-8">
                                    <div class="row ">
                                        <div class="col-md-9">

                                            <div class="herroinner">
                                                <h3>Direct Hiring Solutions</h3>
                                                <h4>The video-based platform enables you to source candidates directly and assess their CV and video profiles. When you identify your ideal candidates you simply invite them to review your opportunity, which can also be supported by videos about your company and vacancy. If the candidate is interested in your opportunity, they will accept the invite making their contact details available for you to progress further.</h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </section>
                <section id="start" class="section  bg-light-5 p-0">
                    <div class="hreo_bred permBg permBg1 directBg1">
                        <div class="container-fluid h-100">
                            <div class="row justify-content-center align-items-center h-100">
                                <div class="col-md-10">
                                    <div class="row justify-content-end">
                                        <div class="col-md-9">

                                            <div class="herroinner text-right">
                                                <h3>How Does It Work?</h3>
                                                <h4>Register with  <a style="color:#f26419" target="_blank" href="https://www.directhiringsolutions.com/">Direct Hiring Solutions</a> using the links provided and when signed in create your profile and vacancy to request this service. When registered you will also be able to review candidates that match your requirements that are already live on the platform.  Network Direct will write and post a confidential advert promoting your role onto appropriate job boards free of charge.  Applications from the advert will be directed to register onto the platform with profiles added to the 'Applied' tab within your vacancy to filter and review.  Applications will be held exclusively against your vacancy for a set period of time enabling you to shortlist and approach candidates directly.  Successful introductions cost as little as £40 making this a highly cost-effective direct hiring service.         </h4>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <div class="newstart">

                    <div class="container">	<div class="text-left"><h2>Benefits</h2></div>
                        <!--<ul class="newlist">-->
                        <!--    <li>   Free confidential un-branded advert created and posted on your behalf</li>-->
                        <!--    <li>	Improved management and control of applications to identify the most suitable candidates	</li>-->
                        <!--    <li>	Only pay when you identify and receive the contact details of shortlisted candidates </li>-->
                        <!--    <li>	Significantly reduce your recruitment costs</li>-->
                        <!--</ul>-->
                        <ul class="newlist">
<li>	Free confidential unbranded advert created and posted on your behalf</li>
<li>	Filter and search applications to identify your specific requirements</li>
<li>	Improve assessment by watching applicants video interviews</li>
<li>	Only engage with candidates that have a genuine interest in your opportunity</li>
<li>	Promote your company & vacancy by video to prospective candidates</li>
<li>	Only pay when you obtain the contact details of shortlisted candidates</li>
</ul>

                    </div>	

                </div>	

                <section id="start" class="section  bg-light-5 p-0">
                    <div class="hreo_bred permBg permBg2 ">

                        <div class="container-fluid  h-100">
                            <div class="row justify-content-center align-items-center h-100">
                                <div class="col-md-8">
                                    <div class="row ">
                                        <div class="col-md-12">

                                            <div class="herroinner  text-right">
                                                <h3>What's next?</h3>
                                                <!--<h4>Click <a href="https://www.directhiringsolutions.com/">here</a> to Register</h4>-->
                                                <!--<h4>Already Registered? <a href="https://www.directhiringsolutions.com/">Login</a></h4>-->
                                                <div class="employerstate">
                                               
                                           <span>Employers:</span> <a href="https://www.directhiringsolutions.com/referral_url/9C3227D64E" style="color: #fff;padding-right: 15px">Register</a>  <a href="https://www.directhiringsolutions.com/referral_url/9C3227D64E" style="color: #fff">Login </a> </div>    <br>
                                           <div class="employerstate employerstate1">
                                               
                                           <span>Candidates:</span>   <a target="_blank" href="<?php echo CANDIDATE_LOGIN ?>" style="color: #fff">Login </a> </div>


                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>


            </div>