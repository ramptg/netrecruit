
<div role="main" class="main">
    <section id="start" class="section service_conslt contact_us bg-light-5 p-0">
        <div class="hreo_bred" style="height: 200px">

            <div class="container-fluid h-100" style="position: relative;">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">

                        <div class="herroinner text-right">
                            <h3>Terms & Conditions</h3>


                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
    <section id="start" class="section diviSection">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">


                                    <p><strong></strong></p>
                                    <p class="text-left">PLEASE READ THESE TERMS AND CONDITIONS  CAREFULLY BEFORE USING THIS SITE</p>
                                    <p><strong>Terms of Website Use</strong></p>
                                    <p>This site is operated by <strong>Network Executive</strong>  Limited.<br>
                                        These terms of use (together with the  documents referred to in them) set out the rules governing your use of our  website ("our site"), whether as a guest or a registered user. Use of  our site includes accessing, browsing, or registering to use our site.<br>
                                        Please read these terms of use carefully  before you start to use our site, as these will apply to your use of our site.  We also recommend that you print a copy for future reference. By using our  site, you confirm that you accept these terms of use and that you agree to  comply with them. If you do not agree to these terms of use, you must not use  our site.<br><br>
                                        <strong>Privacy and Cookies</strong> - These terms of use  supplement (and are in addition to) the terms of our Privacy Policy and our  Cookie Policy. Our Privacy Policy sets out the terms on which we process any  personal data we collect from you or that you provide to us. By using our site,  you consent to such processing and you warrant that all data provided by you is  accurate. Our Cookie Policy sets out information about the cookies we use on  our site.<br>
                                        Other applicable terms - If you purchase  goods or services through our site, the relevant terms and conditions of supply  for the relevant product or service will also apply. These will be clearly  signposted on our site or during the order process.<br>
                                        Changes to these terms of use or to our site  - We may revise these terms of use at any time by amending this page. Please  check this page from time to time to take notice of any changes we made, as  they are binding on you. We may also update our site from time to time, and may  change the content at any time. However, please note that any of the content on  our site may be out of date at any given time and we are under no obligation to  update it. Some of the provisions contained in these terms of use may also be  superseded by provisions or notices published elsewhere on our site. We do not  guarantee that our site, or any content on it, will be free from errors or  omissions.<br><br>
                                        <strong>Accessing our site</strong> - Our site is made  available free of charge. We do not guarantee that our site, or any content on  it, will always be available or be uninterrupted. Access to our site is  permitted on a temporary basis. We may suspend, withdraw, discontinue or change  all or any part of our site without notice. We will not be liable to you if for  any reason our site is unavailable at any time or for any period. You are  responsible for making all arrangements necessary for you to have access to our  site. You are also responsible for ensuring that all persons who access our  site through your Internet connection are aware of these terms of use and other  applicable terms and conditionsand that they comply with them.<br><br>
                                        <strong>Acceptable Use:</strong><br>
                                        - You may use our site only for Lawful  purposes. You may not use our site:<br>
                                        - In any way that breaches any applicable  local, national or international Law or Regulation<br>
                                        - In any way that is unlawful or fraudulent,  or has any unlawful or fraudulent purpose or effect<br>
                                        - For the purpose of harming or attempting to  harm minors in any way<br>
                                        - To send, knowingly receive, upload,  download, use or re-use any material which does not comply with our content  standards<br>
                                        - To transmit, or procure the sending of, any  unsolicited or unauthorised advertising or promotional material or any other  form of similar solicitation (spam)<br>
                                        - To knowingly transmit any data, send or  upload any material that contains viruses, Trojan horses, worms, time-bombs,  keystroke loggers, spyware, adware or any other harmful programs or similar  computer code designed to adversely affect the operation of any computer  software or hardware<br>
                                        You also agree:<br>
                                        -&nbsp; Not  to reproduce, duplicate, copy or re-sell any part of our site in contravention  of these terms of use<br>
                                        -&nbsp; Not  to access without authority, interfere with, damage or disrupt any part of our  site, any equipment or network on which our site is stored, any software used  in the provision of our site or any equipment or network or software owned or  used by any third party</p><br>
                                    <p><strong>Interactive Services</strong> - We may from time to  time provide interactive services on our site, including, without limitation,  chat rooms, bulletin boards and blogs ("interactive services").Where  we do provide any interactive service, we will provide clear information to you  about the kind of service offered, if it is moderated and what form of  moderation is used (including whether it is human or technical). Where we do  moderate an interactive service, we will normally provide you with a means of  contacting the moderator, should a concern or difficulty arise.<br>
                                        We will do our best to assess any possible  risks for users (and in particular, for children) from third parties when they  use any interactive service provided on our site, and we will decide in each  case whether it is appropriate to use moderation of the relevant service  (including what kind of moderation to use) in the light of those risks.  However, we are under no obligation to oversee, monitor or moderate any  interactive service we provide on our site, and we expressly exclude our  liability for any loss or damage arising from the use of any interactive  service by a user in contravention of our content standards, whether the  service is moderated or not.<br>
                                        The use of any of our interactive services by  a minor is subject to the consent of their parent or guardian. We advise  parents who permit their children to use an interactive service that it is  important that they communicate with their children about their safety online,  as moderation is not fool proof. Minors who are using any interactive service  should be made aware of the potential risks to them.<br><br>
                                        <strong>Content Standards</strong> - These content standards  apply to any and all material which you contribute to our site  ("contributions"), and to any interactive services associated with  it. You must comply with the spirit and the letter of the following standards.  The standards apply to each part of any contribution as well as to its whole.  You warrant that any such contribution does comply with the standards listed  below, and you will be liable to us and indemnify us for any breach of this  warranty.<br>
                                        Contributions must be accurate (where they  state facts), be genuinely held (where they state opinions) and comply with  applicable Law in the UK and in any country from which they are posted.<br><br>
                                        <strong>Contributions must not</strong>:<br>
                                        - Contain any material which is defamatory of  any person<br>
                                        - Contain any material which is obscene,  offensive, hateful or inflammatory<br>
                                        - Promote sexually explicit material<br>
                                        - Promote violence<br>
                                        - Promote discrimination based on race, sex,  religion, nationality, disability, sexual orientation or age<br>
                                        - Infringe any copyright, database right or  trade mark of any other person<br>
                                        - Be likely to deceive any person<br>
                                        - Be made in breach of any Legal duty owed to  a third party, such as a contractual duty or a duty of confidence<br>
                                        - Promote any illegal activity<br>
                                        - Be threatening, abuse or invade another's  privacy, or cause annoyance, inconvenience or needless anxiety<br>
                                        - Be likely to harass, upset, embarrass,  alarm or annoy any other person<br>
                                        - Be used to impersonate any person, or to  misrepresent your identity or affiliation with any person<br>
                                        - Give the impression that they emanate from  us, if this is not the case<br>
                                        - Advocate, promote or assist any unlawful  act e.g. copyright infringement or computer misuse</p><br>
                                    <p><strong>Accounts and Passwords</strong> - If you choose, or  you are provided with, a user identification code, password or any other piece  of information as part of our security procedures, you must treat such  information as confidential. You must not disclose it to any third party. We  have the right to disable any user identification code or password, whether  chosen by you or allocated by us, at any time, if in our reasonable opinion you  have failed to comply with any of the provisions of these terms of use. If you  have any concerns regarding your user identification code or password or become  aware of any misuse then you must inform us immediately.<br><br>
                                        <strong>Ownership of Rights</strong> - We are the owner or the  licensee of all intellectual property rights in our siteand in the material  published on it.&nbsp; Those works are  protected by Copyright Laws and Treaties around the world. All such rights are  reserved.<br>
                                        The content on our site is made available for  your personal non-commercial use only and you may only download such content  for the purpose of using this site. You must not use any part of the content on  our site for commercial purposes without obtaining a licence to do so from us  or our licensors. Other than as set out in these terms of use, any other use of  the content on this site is strictly prohibited and you agree not to (and agree  not to assist any third party to) copy, reproduce, transmit, publish, display,  distribute, commercially exploit or create derivative works of such content.<br>
                                        No reliance on information - The content on  our site is provided for general information only. It is not intended to amount  to advice on which you should rely. Although we make reasonable efforts to  update the information on our site, we make no representations, warranties or  guarantees, whether express or implied, that the content on our site is  accurate, complete or uptodate.<br><br>
                                        <strong>Limitation of our Liability</strong> - Nothing in  these terms of use excludes or limits our liability for death or personal  injury arising from our negligence, or our fraud or fraudulent  misrepresentation, or any other liability that cannot be excluded or limited by  English law.<br>
                                        To the extent permitted by Law, we exclude  all conditions, warranties, representations or other terms which may apply to  our site or any content on it, whether express or implied.<br>
                                        We will not be liable to any user for any  loss or damage, whether in contract, tort (including negligence), breach of  statutory duty, or otherwise, even if foreseeable, arising under or in  connection with your use of, or inability to use, our site or your use of or  reliance on any content displayed on it.<br>
                                        If you are a business user, please note that  in particular, we will not be liable for loss of profits, sales, business, or  revenue, business interruption, loss of anticipated savings, loss of business  opportunity, goodwill or reputation or any indirect or consequential loss or  damage.<br>
                                        If you are a consumer user, please note that  we only provide our site for domestic and private use.<br>
                                        You agree not to use our site for any  commercial or business purposes, and we have no liability to you for any loss  of profit, loss of business, business interruption, or loss of business  opportunity.<br>
                                        We will not be liable for any loss or damage  caused by a virus, distributed denial-of-service attack, or other  technologically harmful material that may infect your computer equipment, computer  programs, data or other proprietary material due to your use of our site or to  your downloading of any content on it, or on any website linked to our site.<br>
                                        Different limitations and exclusions of  liability will apply to liability arising as a result of the supply of any  goods or services by us to you, which will be set out in our terms and  conditions of supply.<br><br>
                                        <strong>Viruses</strong> - We do not guarantee that our site  will be secure or free from bugs or viruses. You are responsible for  configuring your information technology, computer programmes and platform in  order to access our site. You should use your own virus protection software.<br>
                                        You must not misuse our site by knowingly  introducing viruses, trojans, worms, logic bombs or other material which is  malicious or technologically harmful. You must not attempt to gain unauthorised  access to our site, the server on which our site is stored or any server,  computer or database connected to our site. You must not attack our site via a  denial-of-service attack or a distributed denial-of service attack.<br>
                                        Third party links and resources on our site -  Where our site contains links to other sites and resources provided by third  parties, these links are provided for your information only and should not be  interpreted as endorsement by us of those linked websites. We have no control  over the contents of those sites or resources and accept no responsibility for  them or for any loss or damage that may arise from your use of them.<br>
                                        Suspension and Termination - We will  determine, in our discretion, whether there has been a breach of these terms of  use through your use of our site. When a breach of this policy has occurred, we  may take such action as we deem appropriate.&nbsp;  Failure to comply with these terms of use may result in our taking all  or any of the following actions:<br>
                                        Immediate, temporary or permanent withdrawal  of your right to use our site.<br>
                                        Immediate, temporary or permanent removal of  any posting/material uploaded by you to our site.<br>
                                        Issue of a warning to you.<br>
                                        Legal proceedings against you for  reimbursement of all costs on an indemnity basis (including, but not limited  to, reasonable administrative and legal costs) resulting from the breach.<br>
                                        Further Legal action against you.<br>
                                        Disclosure of such information to Law  Enforcement Authorities as we reasonably feel is necessary.<br>
                                        We exclude liability for actions taken in  response to breaches of these terms of use.&nbsp;  The responses described in this policy are not limited, and we may take  any other action we reasonably deem appropriate.<br><br>
                                        <strong>Applicable Law </strong>- If you are a consumer,  please note that these terms of use, its subject matter and its formation, are  governed by English Law. You and we both agree to that the courts of England  and Wales will have non-exclusive jurisdiction. If you are a business, these  terms of use, its subject matter and its formation (and any non-contractual  disputes or claims) are governed by English Law. We both agree to the exclusive  jurisdiction of the courts of England and Wales.<br>
                                        Thank you for visiting our site.</p><br>
                                    <p><strong>Network Executive</strong> Limited<br>
                                        The Old Dairy, Western Court, Bishops Sutton, Alresford,  Hampshire, SO24 0AA</p>
                                    <p>Registered in England with Company Number 07768390,VAT  Number 324 8522 07.</p>
                                    <p><strong>Last updated</strong>: 22.07.2019</p>
                                    <p>Terms &amp; Conditions<br>
                                        By using this site you agree to our Terms of  use. Whilst every effort is made to ensure that all information included in our  website is accurate, users are advised that they should take appropriate  precautions to verify such information. <strong>Network Executive</strong> Limited expressly  disclaims all liability for any direct, indirect or consequential loss or  damage occasioned by the user's reliance on any statements, information, or  advice contained in this web site. <strong>Network Executive</strong> Limited is not responsible  for the content of external internet sites.</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>




</div>