<div role="main" class="main">
    <section id="start" class="section service_conslt service_direct  bg-light-5 p-0">
        <div class="hreo_bred">

            <div class="container-fluid h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">

                        <div class="herroinner text-right">
                            <h3><img src="<?php echo base_url() ?>assets/themes/front/images/consultancy-logo-white.png"></h3>
                            <h4><?= $result->slider_content; ?> <br><a href="" style="color:#fff">Register</a> | <a href=""  style="color:#fff">Login</a></h4>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <section id="start" class="section diviSection">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">
                                    <span class="top-sub-title">Divisions</span>
                                    <h2 class="font-weight-bold text-4 mb-3"><?= $result->ftitle; ?></h2>
                                    <p><?= $result->fcontent; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <section id="start" class="section newbgSec  bg-light-5 p-0">
        <div class="hreo_bred permBg directBg permBg05">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row ">
                            <div class="col-md-9">

                                <div class="herroinner">
                                    <h3><?= $result->stitle; ?></h3>
                                    <h4><?= $result->scontent; ?></h4>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg1 directBg1">
            <div class="container-fluid h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row justify-content-end">
                            <div class="col-md-9">

                                <div class="herroinner text-right">
                                    <h3><?= $result->thtitle; ?></h3>
                                    <h4><?= $result->thcontent; ?> </h4>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <div class="newstart">
        <div class="text-center"><h2>Direct Hiring Solutions</h2></div>
        <div class="container">
            <div class="expert-blog">

                <div class="row align-items-center">
                    <div class="col-md-3">
                        <div class="imgBLo">
                            <img class="img-fluid" src="<?php echo base_url() ?>assets/themes/front/images/img11.png">
                        </div>
                    </div>
                    <?php
                    if ($fresult) {
                        foreach ($fresult as $val) {
                            ?>
                            <div class="col-md-3">
                                <div class="imgBLo">
                                    <img class="img-fluid" src="<?php echo base_url('upload/' . $val->file) ?>">
                                </div>
                                <div class="imgTextB">
                                    <?= $val->title; ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>	
    </div>	

    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg2 directBg2">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row ">
                            <div class="col-md-12">

                                <div class="herroinner">
                                    <h3>What's next?</h3>
                                    <h4>Click <a href="">here</a> to Register</h4>
                                    <h4>Already Rigistered? <a href="">Login</a></h4>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


</div>