<div role="main" class="main">
    <section id="start" class="section service_conslt contact_us bg-light-5 p-0">
        <div class="hreo_bred" style="height: 200px">

            <div class="container-fluid h-100" style="position: relative;">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">

                        <div class="herroinner text-right">
                            <h3> Cookie Policy</h3>


                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
    <section id="start" class="section diviSection">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">

                                    <p class="text-center">  When you use our websites, mobile sites, or  other digital services, information may be collected through the use of cookies  and similar technologies.</p>
                                    <p>
                                        <br>
                                        <strong> What are Cookies?</strong><br>
                                        Cookies are small text files which are  downloaded when you visit a website or application. Your browser can read these  files and in some cases remember your preferences, e.g. which content to  display on the screen.<br>
                                        Cookies are not programs and so can't contain  a virus. They are stored in .txt format so you can open them with Notepad or  any other text editor. They usually contain two pieces of information - a site  name and a unique user ID.<br>
                                        Please use the links below to find out more  details on how to manage Cookies from each of the major browsers.<br>
                                        Remember that some of our websites features  won't work with Cookies turned off.</p>
                                    <p class="ptextdecor">
                                        <a target="_blank" href=" https://support.microsoft.com/en-gb/help/17442/windows-internet-explorer-delete-manage-cookies">Internet Explorer</a><br>
                                        <a target="_blank" href=" https://support.microsoft.com/en-gb/help/4468242/microsoft-edge-browsing-data-and-privacy-microsoft-privacy">Edge</a><br>
                                        <a target="_blank" href="https://support.mozilla.org/en-US/kb/cookies-information-websites-store-on-your-computer?redirectlocale=en-US&amp;redirectslug=Cookies">Firefox<br>
                                        </a><a target="_blank" href="https://support.google.com/chrome/answer/95647?hl=en">Google Chrome<br>
                                        </a><a target="_blank" href="https://www.opera.com/help">Opera <br>
                                        </a><a target="_blank" href="https://support.apple.com/en-gb/guide/safari/sfri11471/mac">Safari<br>
                                        </a><a target="_blank" href=" https://support.apple.com/en-gb/HT201265">Safari iOS<br>
                                        </a><a target="_blank" href="https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DAndroid&amp;hl=en">Android</a></p><a target="_blank" href="https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DAndroid&amp;hl=en">

                                    </a>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>






</div>