<div role="main" class="main">
    <section id="start" class="section service_conslt contact_us bg-light-5 p-0">
        <div class="hreo_bred" style="height: 200px">

            <div class="container-fluid h-100" style="position: relative;">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">

                        <div class="herroinner text-right">
                            <h3>Privacy Policy</h3>


                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
    <section id="start" class="section diviSection">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">


                                    <p>If you are looking to make a Data Protection  Request for removal, access or correction please use our Contact Us Page.<br>
                                        This document explains how we use your  personal data.<br>
                                        We are committed to safeguarding the privacy  of our customers and other website visitors.This policy explains how we handle  your personal data.</p>
                                    <strong>1. How  we use your personal data</strong>

                                    <p>This section provides you with details about:<br>
                                        what personal data we may process;
                                        in the case of personal data that we did not  obtain directly from you, where we obtained that data from, and what types of  data we have collected;
                                        the purposes for which we may process your  personal data; and
                                        the Legal grounds on which we process your  data.<br><br>
                                        <strong>Account Data</strong>: We may process the personal  data that you have provided to us when you register so that we can set up your  account with us ("account data"). The account data may include your  name and email address. The account data may be processed for the purposes of  operating our website, providing our services, ensuring the security of our  website and services, maintaining back-ups of our databases and communicating  with you. The legal basis for this processing is our legitimate interests.<br>
                                        We may consider you for opportunities that  you did not specifically apply for but which we think might be a good fit for  your skillset. We may also share your account data with our partner agencies,  and process your account data to facilitate finding you a job. The legal basis  for this processing is our legitimate interests in finding an appropriate  candidate for particular roles and vacancies.<br>
                                        We will then ask you if you would like to be  put forward for such opportunities. If you consent to us doing so, we will pass  your account data to those relevant employers.&nbsp; <br>
                                        Where you have given us your information for  the purposes detailed above, we will enter the account data into our central  recruitment database (“talent pool”). The legal basis for this is our  legitimate interests in maintaining a viable talent pool.<br><br>
                                        <strong>Profile Data</strong>: We may process other  information that you provide to us ("profile data"). This profile  data may include your name, address, telephone number, email address, profile  pictures, gender, date of birth, relationship status, interests and hobbies,  educational details, employment history, curriculum vitae, job preferences and  employment details. The profile data may be processed for the purposes of  enabling and monitoring your use of our website and services. The legal basis  for this processing is our legitimate interests.<br> <br> 
                                        We may also process your profile data in  relation to job vacancies that you have applied for, generally processing any  job applications, facilitating the recruitment process and furthering our  relationship with you. The legal basis for this processing is our legitimate  interests in finding an appropriate person for a particular role.&nbsp; <br>
                                        We may consider you for opportunities that  you did not specifically apply for but which we think might be a good fit for  your skillset. We may also share your profile data with our partner agencies,  and process your account data to facilitate finding you a job. The legal basis  for this processing is our legitimate interests in finding an appropriate  candidate for particular roles and vacancies.<br><br> 
                                        We will then ask you if you would like to be  put forward for such opportunities. If you consent to us doing so, we will pass  your account data to those relevant employers.<br>
                                        Where you have given us consent to process  your information for the purposes detailed above, we will enter the profile  data into our central recruitment database (“talent pool”). The legal basis for  this is our legitimate interests in maintaining a viable talent pool.<br><br>
                                        <strong>Contact Data</strong>: We may collect your details  from third-party sources such as LinkedIn, CV Library or a similar website,  this information may include your name, email address or telephone number  (“contact data”). We may do this where we identify that you are suitable for an  available vacancy. We may use the contact data to contact you to ask whether  you would like us to provide you with recruitment services. Our use of the  contact data in these circumstances is limited to making contact with you to  determine whether you are interested in receiving our services. The legal basis  for this processing is our legitimate interest as a business to maintain a  viable talent pool.<br>
                                        Usage data. We may process data about your  use of our website and services ("usage data"). The usage data may  include your IP address, geographical location, browser type and version,  operating system, referral source, length of visit, page views and website  navigation paths, as well as information about the timing, frequency and  pattern of your service use. The source of the usage data is our analytics  tracking system. This usage data may be processed for the purposes of analysing  the use of the website and services.<br><br>
                                        The Legal basis for this processing is our  legitimate interests, namely monitoring and improving our website and services.<br>
                                        Enquiry data. We may process information  contained in any enquiry you submit to us regarding our services ("enquiry  data"). The enquiry data may be processed for the purposes of offering,  marketing and selling relevant products and/or services to you.<br>
                                        The Legal basis for this processing is  consent.<br><br>
                                        <strong>Transaction Data</strong>: We may process information  relating to any payments made to you through our website ("transaction  data"). The transaction data may include your contact details, your bank  account details, and the transaction details. The transaction data may be  processed for the purposes of processing these payments and keeping proper  records of those transactions.<br>
                                        The Legal basis for this processing is the  performance of a contract between you and us and/or taking steps, at your  request, to enter into such a contract and our legitimate interests, namely our  interest in the proper administration of our website and business.<br><br>
                                        <strong>Notification Data</strong>: We may process information  that you provide to us for the purpose of subscribing to our email  notifications, job alerts and/or newsletters ("notification data").  The notification data may be processed for the purposes of sending you the  relevant notifications, job alerts and/or newsletters.<br>
                                        The Legal basis for this processing is  consent.<br><br>
                                        <strong>Correspondence Data</strong>: We may process  information contained in or relating to any communication that you send to us  ("correspondence data"). The correspondence data may include the  communication content and metadata associated with the communication. Our  website will generate the metadata associated with communications made using  the website contact forms. The correspondence data may be processed for the  purposes of communicating with you and record-keeping.<br>
                                        The Legal basis for this processing is our  legitimate interests, namely the proper administration of our website and  business and communications with users.<br>
                                        Other Processing Activities: In addition to  the specific purposes for which we may process your personal data set out  above, we may also process any of your personal data where such processing is  necessary for compliance with a legal obligation to which we are subject, or in  order to protect your vital interests or the vital interests of another natural  person.<br>
                                        Please do not supply any other person's  personal data to us, unless we prompt you to do so.</p><br>
                                    <strong> 2. Providing  your personal data to others</strong>

                                    <p><a name="_GoBack" id="_GoBack"></a>Our Insurers/Professional  Advisers. We may disclose your personal data to our insurers and/or  professional advisers insofar as reasonably necessary for the purposes of  obtaining and maintaining insurance coverage, managing risks, obtaining  professional advice and managing legal disputes.<br>
                                        Our clients/potential employers/partner Agencies.  We may disclose your profile data and account data to our clients, potential  employers, and our partner agencies insofar as reasonably necessary in relation  to potential job vacancies, and in the process of representing you to employers  who may have an appropriate vacancy for you and administering any job  placement.<br><br>
                                        Where we provide your personal data to any  third party. Where we share your personal data with any third party, we will  ensure this processing is protected by appropriate safeguards including a  suitable data processing agreement with that third party.<br>
                                        To comply with Legal obligations. In addition  to the specific disclosures of personal data detailed above, we may also  disclose your personal data where such disclosure is necessary for compliance  with a legal obligation we have to comply with, or in order to protect your  vital interests or the vital interests of another individual.</p><br>
                                    <strong>
                                        3. Transfers  of your personal data outside of the European Economic Area
                                    </strong>
                                    <p>Where your personal data is transferred  outside of the EEA, we will ensure that either (a) The European Commission has  made an "adequacy decision" with respect to the data protection laws  of the country to which it is transferred, or (b) we have entered into a  suitable data processing agreement with the third party situated in that  country to ensure the adequate protection of your data. Transfers outside of  the EEA will be protected by appropriate safeguards.<br>
                                        You acknowledge that personal data that you  submit for publication through our website or services may be available, via  the internet, around the world. We cannot prevent the use (or misuse) of such personal  data by others.</p><br> 
                                    <strong>4. Retaining  and deleting personal data</strong>
                                    <p>Personal data that we process for any purpose  or purposes shall not be kept for longer than is necessary for that purpose or  those purposes. We will retain and delete your personal data in accordance with  our Data Retention Policy.<br>
                                        We may retain your personal data where such  retention is necessary for compliance with a legal obligation to which we are  subject, or in order to protect your vital interests or the vital interests of  another natural person.</p><br> 
                                    <strong>5. Amendments</strong>
                                    <p>We may update this policy from time to time  by publishing a new version on our website.<br>
                                        You should check this page occasionally to  ensure you are happy with any changes to this policy.<br>
                                        We may notify you of changes to this policy  by email.</p><br> 
                                    <strong>6. Your  Rights</strong>
                                    <p>You may instruct us to provide you with any  personal information we hold about you; provision of such information will be  subject to:</p>
                                    <ul>
                                        <li>Your request not being found  to be unfounded or excessive, in which case a charge may apply; and</li>
                                        <li>The supply of appropriate  evidence of your identity (for this purpose, we will usually accept a photocopy  of your passport certified by a solicitor or bank plus an original copy of a  utility bill showing your current address)</li>
                                        <li>We may withhold personal  information that you request to the extent permitted by Law</li>
                                        <li>You may instruct us at any  time not to process your personal information for marketing purposes</li>
                                        <li>In practice, you will  usually either expressly agree in advance to our use of your personal  information for marketing purposes, or we will provide you with an opportunity  to opt out of the use of your personal information for marketing purposes.The  rights you have under Data Protection Law are:</li>
                                        <li>The  right to access;</li>
                                        <li>The  right to rectification;</li>
                                        <li>The  right to erasure;</li>
                                        <li>The  right to restrict processing;</li>
                                        <li>The  right to object to processing;</li>
                                        <li>The  right to data portability;</li>
                                        <li>The  right to complain to a supervisory authority; and</li>
                                        <li>The  right to withdraw consent.</li>
                                    </ul>
                                    <p>&nbsp;Your  right to access your data. You have the right to ask us to confirm whether or  not we process your personal data and, to have access to the personal data, and  any additional information. That additional information includes the purposes  for which we process your data, the categories of personal data we hold and the  recipients of that personal data. You may request a copy of your personal data.  The first copy will be provided free of charge, but we may charge a reasonable  fee for additional copies.<br>
                                        Your right to rectification. If we hold any  inaccurate personal data about you, you have the right to have these  inaccuracies rectified. Where necessary for the purposes of the processing, you  also have the right to have any incomplete personal data about you completed.<br><br> 
                                        Your right to erasure. In certain  circumstances you have the right to have personal data that we hold about you  erased. This will be done without undue delay. These circumstances include the  following: it is no longer necessary for us to hold those personal data in  relation to the purposes for which they were originally collected or otherwise  processed; you withdraw your consent to any processing which requires consent;  the processing is for direct marketing purposes; and the personal data have  been unlawfully processed. However, there are certain general exclusions of the  right to erasure, including where processing is necessary: for exercising the  right of freedom of expression and information; for compliance with a legal  obligation; or for establishing, exercising or defending legal claims.<br><br> 
                                        Your right to restrict processing. In certain  circumstances you have the right for the processing of your personal data to be  restricted. This is the case where: you do not think that the personal data we  hold about you is accurate; your data is being processed unlawfully, but you do  not want your data to be erased; it is no longer necessary for us to hold your  personal data for the purposes of our processing, but you still require that  personal data in relation to a legal claim; and you have objected to  processing, and are waiting for that objection to be verified. Where processing  has been restricted for one of these reasons, we may continue to store your  personal data. However, we will only process it for other reasons: with your  consent; in relation to a legal claim; for the protection of the rights of  another natural or legal person; or for reasons of important public interest.<br><br> 
                                        Your right to object to processing. You can  object to us processing your personal data on grounds relating to your particular  situation, but only as far as our legal basis for the processing is that it is  necessary for: the performance of a task carried out in the public interest, or  in the exercise of any official authority vested in us; or the purposes of our  legitimate interests or those of a third party. If you make an objection, we  will stop processing your personal information unless we are able to:  demonstrate compelling legitimate grounds for the processing, and that these  legitimate grounds override your interests, rights and freedoms; or the  processing is in relation to a legal claim.</p>
                                    <p>Your right to object to direct marketing. You  can object to us processing your personal data for direct marketing purposes.  If you make an objection, we will stop processing your personal data for this  purpose.<br><br> 
                                        Automated data processing. To the extent that  the legal basis we are relying on for processing your personal data is consent,  and where the processing is automated, you are entitled to receive your  personal data from us in a structured, commonly used and machine-readable  format. However, you may not have this right if it would adversely affect the  rights and freedoms of others.<br><br> 
                                        Complaining to a supervisory authority. If  you think that our processing of your personal data infringes data protection  laws, you can lodge a complaint with a supervisory authority responsible for  data protection. You may do this in the EU member state of your habitual  residence, your place of work or the place of the alleged infringement.<br>
                                        Right to withdraw consent. To the extent that  the legal basis we are relying on for processing your personal data is consent,  you are entitled to withdraw that consent at any time. Withdrawal will not  affect the lawfulness of processing before the withdrawal.<br>
                                        Exercising your rights. You may exercise any  of your rights in relation to your personal data by written notice to us in  addition to the other methods specified above.</p><br> 
                                    <strong>7. Cookie  Policy</strong>
                                    <p>For information about how we use Cookies  please see our Cookie Policy at http://www.networkexecutive.co.uk/legal/cookie-policy/</p><br> 
                                    <strong>8. Our  Details</strong>
                                    <p>This website is owned and operated by Network  Executive Limited.<br>
                                        We are registered in England and Wales under Registration  Number 07768390, VAT Number 324 8522 07.</p>
                                    <p>Our Registered Office and principal place of business is  at The Old Dairy, Western Court, Bishops Sutton, Alresford, Hampshire, SO24 0AA.</p>
                                    <p>You can contact us:</p>
                                    <ul>
                                        <li>By post, using the postal  address given above</li>
                                        <li>Using our website contact  form</li>
                                        <li>By telephone, on the contact  number published on our website from time to time</li>
                                        <li>By email, using the email  address published on our website from time to time</li>
                                    </ul>
                                    <p>If you are looking to make a Data Protection  Request for removal, access or correction please use our Contact Us Page.</p>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>







</div>