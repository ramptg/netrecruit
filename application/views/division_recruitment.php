<div role="main" class="main">
    <section id="start" class="section service_conslt service_direct  bg-light-5 p-0">
        <div class="hreo_bred">

            <div class="container-fluid h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-10">

                        <div class="herroinner text-right">
                            <h3><img src="<?php echo base_url() ?>assets/themes/front/images/net-recruitment-white.png"></h3>
                            <!-- 		<h4>Online solutions that provide direct <br> access  to a talented pool of candidates <br><a href="" style="color:#fff">Register</a> | <a href=""  style="color:#fff">Login</a></h4> -->
                            <h4>Recruitment services that deliver solutions to meet <br>
                                your junior, transactional and part-qualified needs
                            </h4>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <section id="start" class="section diviSection">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row align-items-baseline mt-2">
                        <div class="col-lg-12">
                            <div class="icon-box appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                                <div class="icon-box-info">
                                    <span class="top-sub-title">Divisions</span>
                                    <h2 class="font-weight-bold text-4 mb-3">Net Recruitment</h2>
                                    <p>Net Recruitment deliver recruitment services that provide a fast and effective solution to meet your junior, transactional and part-qualified resourcing needs</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <section id="start" class="section   bg-light-5 p-0">
        <div class="hreo_bred permBg  ">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row ">
                            <div class="col-md-9">

                                <div class="herroinner">
                                    <h3>Permanent, Temporary & Contract Solutions</h3>
                                    <h4>Operating across all functions we deliver permanent, temporary and contract solutions to support your recruitment requirements. Through combining our extensive network with advanced sourcing and search capabilities we provide you with access to a wider pool of candidates to ensure you are assessing the best available talent in the market.</h4>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg1 directBg1">
            <div class="container-fluid h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row justify-content-end">
                            <div class="col-md-9">

                                <div class="herroinner text-right">
                                    <h3>Your recruitment partner</h3>
                                    <h4>Working as your recruitment partner we build strong, long-term relationships that add value to your recruitment activities. We achieve this through providing a dedicated account management team that enables us to gain a strong understanding of your business to enhance communication and effectively promote your employer brand.</h4>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <div class="newstart">
        <div class="text-center"><h2>Multi-functional Expertise</h2></div>
        <div class="container">
            <div class="expert-blog">

                <!-- <div class="row">
                    <?php
                    if ($fresult) {
                        foreach ($fresult as $val) {
                            ?>
                            <div class="col-md-2dot4">
                                <div class="imgBLo">
                                    <img class="img-fluid" src="<?php echo base_url('upload/' . $val->file) ?>">
                                </div>
                                <div class="imgTextB">
                                    <?= $val->title; ?>
                                </div>   
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div> -->
                <div class="row">
                    
                            <div class="col-md-2dot4">
                                <div class="imgBLo">
                                    <img class="img-fluid" src="<?php echo base_url() ?>assets/themes/front/images/headshot1.png">
                                </div>
                                <div class="imgTextB">
                                    Hr Advisor
                                </div>   
                            </div>

                            <div class="col-md-2dot4">
                                <div class="imgBLo">
                                    <img class="img-fluid" src="<?php echo base_url() ?>assets/themes/front/images/headshot2.png">
                                </div>
                                <div class="imgTextB">
                                    Assistant Accountant
                                </div>   
                            </div>

                            <div class="col-md-2dot4">
                                <div class="imgBLo">
                                    <img class="img-fluid" src="<?php echo base_url() ?>assets/themes/front/images/headshot3.png">
                                </div>
                                <div class="imgTextB">
                                   IT Support Analyst              
                                </div>   
                            </div>

                            <div class="col-md-2dot4">
                                <div class="imgBLo">
                                    <img class="img-fluid" src="<?php echo base_url() ?>assets/themes/front/images/headshot4.png">
                                </div>
                                <div class="imgTextB">
                                   Operations Assistant                  
                                </div>   
                            </div>
                            <div class="col-md-2dot4">
                                <div class="imgBLo">
                                    <img class="img-fluid" src="<?php echo base_url() ?>assets/themes/front/images/headshot5.png">
                                </div>
                                <div class="imgTextB">
                                    Procurement Assistant
                                </div>   
                            </div>
                      
                </div>
            </div>
        </div>  
    </div>  
    <section id="start" class="section  bg-light-5 p-0">
        <div class="hreo_bred permBg permBg2">

            <div class="container-fluid  h-100">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-md-8">
                        <div class="row ">
                            <div class="col-md-12">

                                <div class="herroinner text-right">
                                    <h3>What’s next?</h3>
                                    <h4>Contact Us Today</h4>
                                    <p class="details"><strong>United Kingdom</strong> - +44 (0) 1962 677013 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details"><strong>Netherlands</strong> - +31 (0) 20 854 6378 <i class="fas fa-phone-alt"></i></p>
                                    <p class="details">info@netrecruit.com <i class="fas fa-envelope"></i></p>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


</div>