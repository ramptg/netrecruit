<?php

class Basic_model extends CI_model {

    public function __construct() {
        parent::__construct();
    }

    public function getDivisions($slug) {
        $result = $this->db->get_where('manage_network_division', array('slug' => $slug))->row();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function get_multi_fun_expertise($slug) {
        $result = $this->db->get_where('exe_multi_fun_expertise', array('slug' => $slug, 'status' => '0'))->result();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    
    public function geService($slug) {
        $result = $this->db->get_where('manage_services', array('slug' => $slug))->row();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

     public function getTeamContent() {
        $result = $this->db->get_where('team', array('id' => 1))->row();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    
    public function manageTeamData() {
        $result = $this->db->get_where('manage_team_data', array('status' => '1'))->result();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    
     public function getContactContent() {
        $result = $this->db->get_where('contact_content', array('id' => 1))->row();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    
     public function getContactDetail() {
        $result = $this->db->get_where('manage_contact_detail', array('status' => '1'))->result();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    
    public function getPages($slug) {
        $result = $this->db->get_where('pages', array('slug' => $slug))->row();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
}

?>