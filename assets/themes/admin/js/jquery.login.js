var Login_User = function () {
    var process = false;
    var form2 = $("#login_user");
    var error2 = $(".alert-danger", form2);
    var success2 = $(".alert-success", form2);
    var HandleValiddation = function () {

        error2.html('<button class="close" data-close="alert"></button><span>Please Fill All Required Fileds Of Form. </span>');
        success2.html('<button class="close" data-close="alert"></button><span></span>');
        $("#login_user").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                identity: {required: true},
                password: {required: true}

            },
            messages: {
                identity: {required: "Please Enter Username"},
                password: {required: "Please Enter Password"},
            },
            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.input-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label, element) {
                label.hide();
                $(element).closest('.input-group').removeClass('has-error');
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-group'));
            },

            submitHandler: function (form) {
                success2.hide();
                error2.hide();
                process = true;
            }
        });

        $('#login_user input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#login_user').validate().form()) {
                    $('#login_user').submit();
                }
                return false;
            }
        });
        $("#login_user").submit(function (e) {
            e.preventDefault();
           
            if (process) {
                process = false;
                SubmitForm(this);
            }
        });

    }

    var SubmitForm = function (form) {
      
        var Data = $(form).serialize();

        var url = $("#login_user").attr('action'); 
      
       // $('.btn1').attr('disabled','disabled'); 

        var request = $.ajax({type: 'POST', url: url, data: Data});
        $("#loader2").show();
        request.done(function (data) {
            $("#loader2").hide();
            var response = $.parseJSON(data);

            if (response.status == '1') {
                $("#loader2").show();
                setInterval(function () {
                    window.location = response.url;
                   
                }, 4000);
            } else {
                success2.hide();
                $('.alert-danger').html(response.message);
                $('.alert-danger').show();
            }


        });
        request.fail(function () {
            $("#loader2").hide();
            error2.find("span").html("Something Went Wrong Please Try Again");
            error2.show();
        });
    }
    return {init: function () {
            HandleValiddation();
        }};
}();