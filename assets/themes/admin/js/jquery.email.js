var Email_Sent = function () {
    var process = false;
    var form2 = $("#contactdetails");
    var error2 = $(".alert-danger", form2);
    var success2 = $(".alert-success", form2);
    var HandleValiddation = function () {

        error2.html('<button class="close" data-close="alert"></button><span>Please Fill All Required Fileds Of Form. </span>');
        success2.html('<button class="close" data-close="alert"></button><span></span>');
        $("#contactdetails").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {

            },
            messages: {

            },
            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label, element) {
                label.hide();
                $(element).closest('.form-group').removeClass('has-error');
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.form-group'));
            },

            submitHandler: function (form) {
                success2.hide();
                error2.hide();
                process = true;
            }
        });

        $('#contactdetails input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#contactdetails').validate().form()) {
                    $('#contactdetails').submit();
                }
                return false;
            }
        });
        $("#contactdetails").submit(function (e) {
            e.preventDefault();

            if (process) {
                process = false;
                SubmitForm(this);
            }
        });
    }

    var SubmitForm = function (form) {
        var Data = $(form).serialize();
        var url = $("#contactdetails").attr('action');

        var request = $.ajax({type: 'POST', url: url, data: Data});
        $("#loader2").show();
        request.done(function (data) {
            $("#loader2").hide();
            var response = $.parseJSON(data);

            if (response.status == '1') {
                $('#contactdetails')[0].reset();
                error2.hide();
                success2.show();
                $('.alert-success').html(response.message);
                $('.alert-success').show();
            } else {
                success2.hide();
                $('.alert-danger').html(response.message);
                $('.alert-danger').show();
            }
        });
        request.fail(function () {
            $("#loader2").hide();
            error2.find("span").html("Something Went Wrong Please Try Again");
            error2.show();
        });
    }
    return {init: function () {
            HandleValiddation();
        }};
}();