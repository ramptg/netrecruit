var AssignLoan = function () {
    var process = false;
    var form2 = $("#assign_loan_data");
    var error2 = $(".alert-danger", form2);
    var success2 = $(".alert-success", form2);
    var HandleValiddation = function () {
        
        $.validator.addMethod('ge', function(value, element, param) {
             return this.optional(element) || parseInt(value) >= parseInt($(param).val());
        }, 'Invalid value');
    
        error2.html('<button class="close" data-close="alert"></button><span>Please Fill All Required Fileds Of Form. </span>');
        success2.html('<button class="close" data-close="alert"></button><span></span>');
        $("#assign_loan_data").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                loan_amount: {ge: '#doc_charges'}
            },
            messages: {
                loan_amount: {ge: 'Must be greater than or equal to document charges'},
            },
            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label, element) {
                label.hide();
                $(element).closest('.form-group').removeClass('has-error');
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.form-group'));
            },

            submitHandler: function (form) {
                success2.hide();
                error2.hide();
                process = true;
            }
        });

        $('#assign_loan_data input').keypress(function (e) {
            if (e.which == 13) {
                if ($('#create_loan').validate().form()) {
                    $('#create_loan').submit();
                }
                return false;
            }
        });
        $("#assign_loan_data").submit(function (e) {
            e.preventDefault();
            if (process) {
                process = false;
                SubmitForm(this);
            }
        });

    }

        
        
    var SubmitForm = function (form) {

        var Data = $(form).serialize();
        var url = $("#assign_loan_data").attr('action');
        var request = $.ajax({
            type: 'POST',
            url: url, 
            data: new FormData(form), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false,
            
        });
        $("#loaderjob").show();
        request.done(function (data) {
            $("#loaderjob").hide();
            var response = $.parseJSON(data);
            if (response.status == '1') {
                alert(response.message);
                window.location.href=response.url;
                error2.hide();
                $('.alert-success').html(response.message);
                $('.alert-success').show();
            } else {
                alert(response.message);
               // window.location.reload(true);
                success2.hide();
                $('.alert-danger').html(response.message);
                $('.alert-danger').show();
            }


        });
        request.fail(function () {
            $("#loader").hide();
            error2.find("span").html("Something Went Wrong Please Try Again");
            error2.show();
        });
    }
    return {init: function () {
            HandleValiddation();
        }};
}();